<?php
require "pdo/config.php";
//$alias = "/".$_SERVER['QUERY_STRING'];
$alias = $_SERVER['REQUEST_URI'];
try  {
    $connection = new PDO($dsn, $username, $password, $options);
    $sql = "SELECT * 
            FROM pages
            WHERE menu = :menu
            AND parent = :parent";
    $parent = 0;
    $menu = 1;
    $statement = $connection->prepare($sql);
    $statement->bindParam(':parent', $parent, PDO::PARAM_STR);
    $statement->bindParam(':menu', $menu, PDO::PARAM_STR);
    $statement->execute();
    $result = $statement->fetchAll();
} catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
$menu_first = [];
if ($result && $statement->rowCount() > 0) {
    $menu_first = $result;
}
$menu_second = [];
function menuSecondAll(){
    require "pdo/config.php";
    try  {
        $connection = new PDO($dsn, $username, $password, $options);
        $sql = "SELECT * 
            FROM pages
            WHERE menu = :menu
            AND parent > :parent";
        $parent = 0;
        $menu = 1;
        $statement = $connection->prepare($sql);
        $statement->bindParam(':parent', $parent, PDO::PARAM_STR);
        $statement->bindParam(':menu', $menu, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
    $menu_second = [];
    if ($result && $statement->rowCount() > 0) {
        $menu_second = $result;
    }
    return $menu_second;
}
function menuSecond($id){
    require "pdo/config.php";
    try  {
        $connection = new PDO($dsn, $username, $password, $options);
        $sql = "SELECT * 
            FROM pages
            WHERE menu = :menu
            AND parent = :parent";
        $parent = $id;
        $menu = 1;
        $statement = $connection->prepare($sql);
        $statement->bindParam(':parent', $parent, PDO::PARAM_STR);
        $statement->bindParam(':menu', $menu, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
    $menu_second = [];
    if ($result && $statement->rowCount() > 0) {
        $menu_second = $result;
    }

    return $menu_second;
}
function menuSecondByAlias($alias){
    require "pdo/config.php";
    try  {
        $connection = new PDO($dsn, $username, $password, $options);
        $sql = "SELECT * 
            FROM pages
            WHERE menu = :menu
            AND alias LIKE :alias";
        $menu = 1;
        $alias = $alias."%";
        $statement = $connection->prepare($sql);
        $statement->bindParam(':menu', $menu, PDO::PARAM_STR);
        $statement->bindParam(':alias', $alias, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
    $menu_second = [];
    if ($result && $statement->rowCount() > 0) {
        $menu_second = $result;
    }

    return $menu_second;
}
$fasad_array = [
            '/fasad2.php' =>   'сделанные объекты',
            '/fasad3.php' =>   'чем мы лучше',
            '/fasad4.php' =>   'стоимость',
            '/fasad5.php' =>   'важная информация заказчику'
];
$parent_id = 0;
$menu_sec = menuSecondAll();
foreach ($menu_sec as $menu_item) {
    if ($_SERVER['REQUEST_URI'] == $menu_item['alias']) {
//        $menu_second = menuSecond($menu_item['parent']);
        $parent_id = $menu_item['parent'];
    }
}
?>
<div class="row">
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" id="navbar_button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li height="38" <?php echo ($_SERVER['REQUEST_URI'] == '/')?'class="active"':'';?>>
                    <a href="/">НА ГЛАВНУЮ</a>
                </li>
                <li <?php if(substr($_SERVER['REQUEST_URI'], 0,7) == '/ofirme'){
                    echo 'class="active"';
                    $menu_second = menuSecondByAlias('/ofirme');
                }?>>
                    <a href="ofirme.php">О КОМПАНИИ</a>
                </li>
                <li <?php if(substr($_SERVER['REQUEST_URI'], 0,5) == '/news'){
                    echo 'class="active"';
                $menu_second = menuSecondByAlias('/news');
                }?>>
                    <a href="news.php">НОВОСТИ</a>
                </li>
                <li <?php if(substr($_SERVER['REQUEST_URI'], 0,7) == '/proekt'){
                    echo 'class="active"';
                $menu_second = menuSecondByAlias('/proekt');
                }?>>
                    <a href="proekt.php">ПРОЕКТИРОВАНИЕ</a>
                </li>
                <li <?php if(substr($_SERVER['REQUEST_URI'], 0,13) == '/stroitelstvo'){
                    echo 'class="active"';
                $menu_second = menuSecondByAlias('/stroitelstvo');
                }?>>
                    <a href="stroitelstvo.php">СТРОИТЕЛЬСТВО</a>
                </li>
                <li <?php if(substr($_SERVER['REQUEST_URI'], 0,6) == '/fasad'){
                    echo 'class="active"';
                $menu_second = menuSecondByAlias('/fasad');
                    foreach ($fasad_array as $key=>$value){
                        $new_item['alias'] = $key;
                        $new_item['title'] = $value;
                        $menu_second[] = $new_item;
                    }
                }?>>
                    <a href="fasad.php">ФАСАДНЫЕ РАБОТЫ</a>
                </li>
                <li <?php if(substr($_SERVER['REQUEST_URI'], 0,9)  == '/contacts'){
                    echo 'class="active"';
                $menu_second = menuSecondByAlias('/contacts');
                }?>>
                    <a href="contacts.php">КОНТАКТЫ</a>
                </li>
                <?php
                    foreach ($menu_first as $item) :
                ?>
                <li <?php
                        if($_SERVER['REQUEST_URI'] == $item['alias'] || $parent_id == $item['id']){
                            echo 'class="active"';
                            $menu_second = menuSecond($item['id']);
                        }
                    ?>>
                    <a href="<?=$item['alias']?>"><?=$item['title']?></a>
                </li>
                <?php
                    endforeach;
                ?>
            </ul>
        </div>
    </div>

    <ul class="navbar-nav second" id="second_level_menu">
        <?php foreach ($menu_second as $item) : ?>
            <li class="second-level <?php echo ($_SERVER['REQUEST_URI'] == $item['alias'])?'active':'';?>">
                <a href="<?=$item['alias']?>"><?=$item['title']?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>
</div>
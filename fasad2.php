<?php require 'header.php'; ?>
    <h4 align="center">Фотографии некоторых сделанных нами фасадов</h4>
<div class="row fasad2">
    <h4 align="center" class="bg-grey">Администрация сельского поселения (1998-1999 гг)</h4>
</div>
<div class="col-sm-12">
    <div class="col-sm-12">
        <div class="col-md-6">
            <a href="photogallery/facades/avinur1.jpg" rel="lightbox">
                <img class="fotofon fotofon-big mob-respons" src="photogallery/facades/avinur1m.jpg">
            </a>
        </div>
        <div class="col-md-6">
            <a href="photogallery/facades/avinur2.jpg" rel="lightbox">
                <img class="fotofon fotofon-big mob-respons" src="photogallery/facades/avinur2m.jpg">
            </a>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="col-md-6">
            <a href="photogallery/facades/avinur3.jpg" rel="lightbox">
                <img class="fotofon fotofon-big mob-respons" src="photogallery/facades/avinur3m.jpg">
            </a>
        </div>
        <div class="col-md-6">
            <a href="photogallery/facades/avinur4.jpg" rel="lightbox">
                <img class="fotofon fotofon-big mob-respons" src="photogallery/facades/avinur4m.jpg">
            </a>
        </div>
    </div>


    <div class="col-sm-12">
        <div class="col-md-6 second_home">
            <iframe src="https://www.youtube.com/embed/p4GY5gXB3Yo?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="col-md-6">
            <p class="fs-18">Данный объект начали осенью 1998 г и закончили в 1999 году. <strong>На основе этого объекта можно отследить долговечность выполняемых нами фасадных работ.</strong><br>
                В 2015 году фактическое состояние фасада было отснято на видео и этот фрагмент вошёл в фильм по технологии теплоизоляции фасадов.
            <p> <span class="red-V12">Вы можете убедиться, что никаких проблем по вине именно фасадных работ, как таковых, спустя 17 лет на фасаде не имеется.</span><br>
                <br>
                Кликнув <a href="https://youtu.be/p4GY5gXB3Yo?t=4166" target="_blank">на данную ссылку</a>, показ (на ютубе) фильма начинается именно с этого фрагмента.</p>
        </div>
    </div>
    <hr>
</div>
<div class="fasad2-block"></div>
<div class="row fasad2">
    <h4 align="center" class="bg-grey">Различные объекты (1997-2000 гг)</h4>
</div>

<div class="col-sm-12 mt-50" align="center">
    <div class="col-md-3"><a href="photogallery/facades/portf_fas_estcot.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/portf_fas_estcot_m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/facades/portf_fas_kdk_1.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/portf_fas_kdk_1m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/eesti/maksuamet7.jpg" rel="lightbox"><img class="fotofon" src="photogallery/eesti/maksuamet7m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/facades/portf_fas_prn_.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/portf_fas_prn_m.jpg"></a></div>
</div>
<div class="col-sm-12" align="center">
    <div class="col-md-3"><a href="photogallery/facades/portf_fas_prn-energ.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/portf_fas_prn-energ_m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/facades/portf_fas_prn-mnt2.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/portf_fas_prn-mnt2m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/facades/betooni4.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/betooni4m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/facades/loo2.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/loo2m.jpg"></a></div>
</div>
<div class="fasad2-block"></div>
<div class="row fasad2">
    <h4 align="center" class="bg-grey">Изготовление декоративного оформления - барельефов на здании казино. (1999 г)</h4>
</div>
<div class="col-sm-12 mt-50" align="center">
    <div class="div">
    <div class="col-md-4"><a href="photogallery/facades/barelyef1.jpg" rel="lightbox"><img class="fotofon"  src="photogallery/facades/barelyef1m.jpg"></a></div>
    <div class="col-md-4"><a href="photogallery/facades/barelyef2.jpg" rel="lightbox"><img class="fotofon"  src="photogallery/facades/barelyef2m.jpg"></a></div>
    <div class="col-md-4"><a href="photogallery/facades/barelyef3.jpg" rel="lightbox"><img class="fotofon"  src="photogallery/facades/barelyef3m.jpg"></a></div>
    </div>
</div>


    <div class="fasad2-block"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey">Коттедж, Московская область, Дмитровский район, коттеджный посёлок &quot;Зелёный мыс&quot; (2005 г.)</h4>
    </div>
    <div class="col-sm-12 mt-50" align="center">
        <div class="col-md-3"><a href="photogallery/facades/zel_1.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_1m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zel_2.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_2m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zel_3.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_3m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zel_4.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_4m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/zel_5.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_5m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zel_6.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_6m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zel_7.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_7m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zel_8.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_8m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/zel_10.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_10m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zel_11.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_11m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zel_12.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_12m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zel_13.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zel_13m.jpg"></a></div>
    </div>


    <div class="fasad2-block"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey">Коттеджи, Ленинградская область (2008-2009 гг)</h4>
    </div>
    <div class="col-sm-12 mt-50" align="center">
        <div class="col-md-3"><a href="photogallery/facades/sosn3.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/sosn3m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/sosn4.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/sosn4m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/sosn1.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/sosn1m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/sosn2.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/sosn2m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/vsevol3.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vsevol3m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/vsevol4.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vsevol4m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/vsevol1.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vsevol1m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/vsevol.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vsevol_m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/vartem11.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vartem11m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/vartem12.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vartem12m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/vartem13.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vartem13m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/vartem1.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vartem1m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/vartem21.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vartem21m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/vartem23.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vartem23m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/vartem22.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vartem22m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/vartem2.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vartem2m.jpg"></a></div>
    </div>

    <div class="fasad2-block"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey">Многоэтажный жилой дом. г. Санкт-Петербург, ул. Передовиков 9 к.2 (2009 г.)</h4>
    </div>
    <div class="col-sm-12 mt-50" align="center">
        <div class="col-md-3"><a href="photogallery/facades/spb_pered1.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/spb_pered1m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/spb_pered2.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/spb_pered2m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/spb_pered3.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/spb_pered3m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/spb_pered4.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/spb_pered4m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/spb_pered5.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/spb_pered5m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/spb_pered6.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/spb_pered6m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/spb_pered7.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/spb_pered7m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/spb_pered.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/spb_pered_m.jpg"></a></div>
    </div>

    <div class="fasad2-block"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey">г. Вологда. Ремонт фасадов по государственной программе 185 ФЗ. (2009 г.)</h4>
    </div>

    <div class="col-sm-12 mt-50" align="center">
        <div class="col-md-4"><a href="photogallery/facades/vologda6.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vologda6m.jpg"></a></div>
        <div class="col-md-4"><a href="photogallery/facades/vologda2.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vologda2m.jpg"></a></div>
        <div class="col-md-4"><a href="photogallery/facades/vologda3.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vologda3m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-4"><a href="photogallery/facades/vologda4.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vologda4m.jpg"></a></div>
        <div class="col-md-4"><a href="photogallery/facades/vologda5.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vologda5m.jpg"></a></div>
        <div class="col-md-4"><a href="photogallery/facades/vologda1.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/vologda1m.jpg"></a></div>
    </div>

    <div class="col-sm-12 mt-50">
        <div class="col-md-6 second_home mt-50">
            <iframe src="https://www.youtube.com/embed/NFYz39KZ1JY?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="col-md-6">
            <span class="fs-18"><p>Выпуск новостей г. Вологда 21 октября 2009 г.<br>
                Первый дом в Вологде, выполненный по реставрационной технологии, реставрационными материалами австрийского производства. И первый дом в Вологде с прогрессивным цветовым решением фасада.</p>
            <p>Привлекли внимание Администрации города и телевизионщиков.</p>
            <p><strong>До сих пор, как надо Вологодской Администрации (области и города) перед какими &quot;столичными&quot; комиссиями выпендрится, как хорошо строят в Вологде, везут их именно к этому дому.</strong><br>
                Вот только беда - я сделал два дома и больше в том мутном болоте работать не хочется. </p>
            <p>Пусть местные кривые руки дальше портят всё, к чему прикасаются.</p></span>
        </div>
    </div>
    <div class="col-sm-12 mt-50">
        <div class="col-md-6 second_home">
            <iframe src="https://www.youtube.com/embed/YUM-2NJBKO8?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="col-md-6">
            <span class="fs-18"><p>Ещё один выпуск новостей.</p>
                <p>Комиссия во главе с мэром Вологды катались по объектам капремонта.<br>
                Приехали на соседний дом, назвали его &quot;идеальным&quot;, после зимы фасадная отделка этого &quot;идеального дома&quot; начала сыпаться.<br>
                Наш скромный, но симпатичный дом - рядом, потому и к нам заглянули.</p></span>
        </div>
    </div>


    <div class="fasad2-block"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey">Коттедж, г. Домодедово Московская область (2012)</h4>
    </div>

    <div class="col-sm-12 mt-50" align="center">
        <div class="col-md-3"><a href="photogallery/facades/domod1.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod1m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod3.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod3m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod4.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod4m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/domod5.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod5m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod6.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod6m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod7.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod7m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod8.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod8m.jpg"></a></div>
    </div>



    <div class="fasad2-block"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey">Коттедж, Домодедовский район, Московская область (2013)</h4>
    </div>

    <div class="col-sm-12 mt-50" align="center">
        <div class="col-md-3"><a href="photogallery/facades/domod2-01.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-01m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-02.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-02m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-03.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-03m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-04.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-04m.jpg"></a></div>
    </div>

    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/domod2-05.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-05m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-06.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-06m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-07.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-07m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-08.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-08m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/domod2-09.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-09m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-10.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-10m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-11.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-11m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-12.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-12m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/domod2-13.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-13m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-14.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-14m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-15.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-15m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-16.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-16m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/domod2-17.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-17m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-18.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-18m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-19.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-19m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/domod2-20.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/domod2-20m.jpg"></a></div>
    </div>

    <div class="col-sm-12 mt-50">
        <div class="col-md-6 second_home">
            <iframe src="https://www.youtube.com/embed/pwDfJHNR8dI?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="col-md-6 mt-50">
            <span class="fs-18"><p>На этом объекте снял  небольшой видеоролик, за качество изготовления которого уже
                    немного стыдно, так как снято по-быстрому, на фотоаппарат без штатива и сейчас по видеосъёмке и
                    постпродакшену уже совершенно другой, более высокий уровень.</p></span>
        </div>
    </div>



    <div class="fasad2-block"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey">Коттедж, г. Зеленогорск Ленинградская область (2013)</h4>
    </div>

<div class="col-sm-12">
    <span class="mt-50">
        <p>На этом объекте какими-то фасадчиками, которые с первого взгляда, были &quot;хорошие&quot; (заказчик адекватный,
            общается, выясняет всё подробно), получился фасад, который простоял совсем недолго.<br>
              Просто не повезло.<br>
              Предложили для финишной отделки &quot;новый материал&quot;, который не имеет должной сертификации и производство
            его какой-то &quot;дерибас&quot;. Это так называемая &quot;мозаичная штукатурка&quot; - кварцевый песок в акриловом связующем.<br>
              Вот только никто не подумал, что на газобетонные стены, утеплённые ватой, нельзя ставить запирающую влагу
            внутри стен акриловую штукатурку. Я, кстати,
            <a href="http://www.wdvs.ru/statyi-gennadia-emelyanova/problema-paropronitsaemosti-v-sistemah.html"
               target="_blank"> об этом ещё писал</a> в журнале &quot;Лучшие фасады&quot; в 2006 г.<br>
              По этой причине начал сыпаться базовый и декоративный слой, как правило, на причину запирания влаги
            указывают характерные особенности - разрушение начинается с мест, где присутствуют &quot;мостики холода&quot;
            (идёт внутренний конденсат и увлажнение) и наиболее короткий выход влаги изнутри-наружу (откосы).<br>
              Ещё с самим материалом проблемы - песок некачественный, в выработках присутствует много железа, чтобы
            тщательно произвести качественный материал необходимо учитывать много факторов и контролировать сырьё в
            химлаборатории. Здесь примесь железа была не замечена из-за кустарного производства и со временем на фасаде
            появились бурые пятная ржавчины - вкрапления железа корродировали.<br>
              Осыпающиеся откосы пробовали немного подправлять декорштукатуркой.<br>
              Сам фасад был сделан хоть ровно и на вид нормально, но технологически не качественно. Два слоя минваты,
            (даже) никак не склеены (хотя надо одним слоем делать, сразу видно, что это привычка работы из вентфасадной
            технологии), дюбеля хорошие, а вот у сетки нет должного нахлёста в 10 см. На откосах раствора порядка 20 мм,
            что недопустимо. Какая-то не понятно для чего сетка-серпянка для внутренних работ поверх фасадной.</p>
              <p>Заказчик решил переделать фасад.<br>
                Произведён демонтаж. Минвату не удалось сохранить. Примерно 30-40% минваты годится только в какое-то
                  утепление, например, кровли или каркаса. Остальное пришлось вывезти на свалку.</p>

    </span>
</div>


    <div class="col-sm-12 mt-50" align="center">
        <div class="col-md-3"><a href="photogallery/facades/zelenog01.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog01m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog02.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog02m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog03.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog03m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog04.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog04m.jpg"></a></div>
    </div>
    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/zelenog05.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog05m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog06.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog06m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog07.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog07m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog08.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog08m.jpg"></a></div>
    </div>

<div class="col-sm-12">
    <spam class="mt-50">
        <p>                    Материал для нового фасада был куплен весь новый. </p>
        <p>Минвата 120 мм, финишный слой - декоративная силиконовая штукатурка.<br>
            Заказчик захотел сделать отливы улучшенного качества со специальными загибами. Непосредственно с их
            изготовлением всегда проблемы - подавляющее большинство фирм такие просто не умеет изготавливать.<br>
            Только одна фирма делает эти отливы отлично и заслуживает специального упоминания - это
            <a href="http://eurogib.ru" target="_blank">http://eurogib.ru/</a> Фирма имеет мастерские в
            г. Санкт-Петербург и Москва. </p>
    </spam>
</div>



    <div class="col-sm-12 mt-50" align="center">
        <div class="col-md-3"><a href="photogallery/facades/zelenog09.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog09m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog10.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog10m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog11.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog11m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog12.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog12m.jpg"></a></div>
    </div>

    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/zelenog13.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog13m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog14.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog14m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog15.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog15m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog16.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog16m.jpg"></a></div>
    </div>

    <div class="col-sm-12" align="center">
        <div class="col-md-3"><a href="photogallery/facades/zelenog17.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog17m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog18.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog18m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog19.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog19m.jpg"></a></div>
        <div class="col-md-3"><a href="photogallery/facades/zelenog20.jpg" rel="lightbox"><img class="fotofon" src="photogallery/facades/zelenog20m.jpg"></a></div>
    </div>



    <div class="fasad2-block"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey">Коттедж, Смоленская область (2014)</h4>
    </div>
    <h4 align="center">На этом коттедже был снят фильм по технологии выполнения фасадных декоров</h4>

    <div class="col-sm-10 col-sm-offset-1">
        <div class="ofirme-video" align="center">
            <br>
            <iframe src="https://www.youtube.com/embed/D2ST8TEqCCQ?rel=0" frameborder="0" allowfullscreen=""></iframe>
        </div>
    </div>

    <div class="fasad2-block mt-50"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey h2-fasad2 mt-50">Коттедж, г. Павловск, г. Санкт-Петербург (2014)</h4>
    </div>
    <div class="fasad2-block"></div>
    <div class="row fasad2">
        <h4 align="center" class="bg-grey">Коттедж, ст. Пери, Ленинградская область (2014)</h4>
    </div>

    <h4 align="center">На этих двух коттеджах был снят фильм по технологии &quot;СФТК&quot; (&quot;мокрый фасад&quot;)</h4>
    <div class="col-sm-10 col-sm-offset-1">
        <div class="ofirme-video" align="center">
            <br>
            <iframe src="https://www.youtube.com/embed/p4GY5gXB3Yo?rel=0" frameborder="0" allowfullscreen=""></iframe>
        </div>
    </div>
    <div class="col-sm-12 fasad-next" align="center"><a href="fasad3.php">&gt; на следующую страницу &quot;Чем мы лучше&quot;</a></div>
<?php require 'footer.php'; ?>
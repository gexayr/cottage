<?php require 'header.php'; ?>
<div class="row">
    <div class="col-sm-12 style1">
        <span>Новости</span>
    </div>
</div>
<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">июль 2016</span>
        </div>
    </div>
    <div class="col-sm-8">
        <p>Выполняются фасадные работы по утеплению и отделке коттеджа в Ленинградской области. Теплоизоляция 150 мм
            пенополистирол.</p>
    </div>
</div>

<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">июнь 2016</span>
        </div>
    </div>
    <div class="col-sm-8">
        <p>Выполнены фасадные работы по утеплению и отделке коттеджа в г. Челябинске. Теплоизоляция 200 мм
            пенополистирол. Система СФТК - KREISEL.
            Декоративная штукатурка - силикатная.</p>
    </div>
</div>

<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">март 2016</span>
        </div>
    </div>
    <div class="col-sm-8">
        <p>Для компании &quot;<a href="http://termoclip.ru/" target="_blank">Термоклип</a>&quot; будут сняты и
            изготовлены несколько корпоративных роликов в 2016 году.</p>
    </div>
</div>

<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">февраль 2016</span>
        </div>
    </div>
    <div class="col-sm-8">
        <p>С компанией &quot;<a href="http://www.sibur.ru" target="_blank">СИБУР</a>&quot; подписан Договор о съёмке и
            производстве в 2016 г. очередного фильма по строительным технологиям.<br>
            Как правильно строить из несъёмной опалубки из пенополистирола.<br></p>
        <span class="red">Ищем объект, где можно отснять эту технологию. </span>Заказчикам предлагаем высокое качество
        работ, чистоту и аккуратность на объекте, соблюдение всех строительных норм и правил, а также бонусы за
        терпение.<br>
        <br>
    </div>
</div>

<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">февраль 2016</span>
        </div>
    </div>
    <div class="col-sm-8">
        <p>С компанией &quot;<a href="http://www.sibur.ru" target="_blank">СИБУР</a>&quot; подписан Договор о съёмке и
            производстве в 2016 г. очередного фильма по строительным технологиям.<br>
            Пошаговая инструкция строительства фундамента &quot;УШП&quot; (&quot;Утеплённая Шведская Плита&quot;).</p>
        <p>Съёмки выполняются на объекте компании <a href="http://baubild.ru/" target="_blank">BAUBILD</a> .
            Пенополистирол предоставляет компания <a href="http://www.ms31.ru/"
                                                     target="_blank">&quot;МОССТРОЙ-31&quot;</a>.<br>
        </p>
    </div>
</div>


<div class="col-sm-12 news-block">
    <hr class="hr">
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">октябрь-ноябрь 2015</span>
        </div>
        <a href="photogallery/news/barviha.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/barviha_m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Начаты работы по утеплению и отделке фасада с помощью декоративных элементов в пос. Барвиха Одинцовского
            района Московской области.<br>
            Теплоизоляция - пенополистирол , система STO, декоративная штукатурка - силиконовая.
            <br>
            Работы будут продолжены в 2016 г.</p>
        </p>
    </div>
</div>



<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">август-декабрь 2015</span>
        </div>
        <a href="photogallery/news/osp.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/osp_m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Сделан проект и построен небольшой каркасный гостевой коттедж в Сергиево-Посадском районе Московской области.<br>
            Фундамент - &quot;Утеплённый Финский Фундамент&quot; (бетонная лента на слое из пенополистирольных плит). <br>
            На этом объекте снят и смонтирован обучающий фильм по технологии строительства, доступен на ютубе:
            <a href="http://www.youtube.com/watch?v=SocctaXFRwY" target="_blank">http://www.youtube.com/watch?v=SocctaXFRwY</a><br>
            <br>
            В 2016 году продолжится отделка - будет сделан фасад по технологии CERESIT VISAGE.</p>
        </p>
    </div>
</div>



<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">май-июнь 2015</span>
        </div>
        <a href="photogallery/news/leningr.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/leningr.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Сделано утепление фасада коттеджа из керамзитобетонных блоков в Зеленоградском районе Московской области.<br>
            Теплоизоляция - пенополистирол, система STO, декоративная штукатурка - силиконовая.</p>
        </p>
    </div>
</div>


<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">сентябрь-октябрь 2014</span>
        </div>
        <a href="photogallery/news/peri0.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/peri0m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Сделано утепление фасада каркасного коттеджа по ОСП-плитам ст. Пери Ленинградская область.<br>
            Теплоизоляция - пенополистирол , система BAUMIT, декоративная штукатурка - силиконовая.<br>
            <br>
            На данном коттедже снят обучающий фильм по технологии СФТК (&quot;мокрый фасад&quot;), доступный на ютубе:
            <a href="http://www.youtube.com/watch?v=p4GY5gXB3Yo" target="_blank">http://www.youtube.com/watch?v=p4GY5gXB3Yo</a></td>
        </p>
        </p>
    </div>
</div>

<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">август-сентябрь 2014</span>
        </div>
        <a href="photogallery/news/pavlovsk.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/pavlovsk_m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Сделано утепление фасада коттеджа из газобетона марки D600 в г. Павловск, г. Санкт-Петербург.<br>
            Теплоизоляция - пенополистирол, система KREISEL, декоративная штукатурка - силиконовая.<br>
            <br>
            На данном коттедже снят обучающий фильм по технологии СФТК (&quot;мокрый фасад&quot;), доступный на ютубе:
            <a href="http://www.youtube.com/watch?v=p4GY5gXB3Yo" target="_blank">http://www.youtube.com/watch?v=p4GY5gXB3Yo</a>
        </p>
    </div>
</div>

<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">июнь 2014</span>
        </div>
        <a href="photogallery/news/sestroretsk.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/sestroretsk.jpg" >
        </a>
    </div>
    <div class="col-sm-8">
        <p>
        Сделано утепление фасада коттеджа из керамического кирпича в г. Сестрорецке Ленинградской области<br>
        Теплоизоляция - минеральная вата &quot;Paroc&quot;, система CERESIT, декоративная штукатурка - силиконовая.
        </p>
    </div>
</div>

<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">июнь-октябрь 2014</span>
        </div>
        <a href="photogallery/news/smolensk1.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/smolensk1m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Продолжены работы по строительству и отделке коттеджа в Смоленской области. За сезон сделана крыша
            (металлочерепица Метробонд), фасад и декоративные элементы на фасаде.<br>
            На фасаде теплоизоляция - пенополистирол, система KREISEL, декоративная штукатурка - силиконовая.<br>
            <br>
            На этом объекте снят и смонтирован обучающий фильм по технологии производства декорэлементов на объекте,
            доступен на ютубе: <a href="http://www.youtube.com/watch?v=D2ST8TEqCCQ" target="_blank">http://www.youtube.com/watch?v=D2ST8TEqCCQ</a></td>

        </p>
    </div>
</div>




<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">2014</span>
        </div>
        <a href="photogallery/news/aes.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/aes.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Выполнялись работы по отделке внутренних помещений на территории атомной электростанции в Сосновом Бору (Ленинградская область)
        </p>
    </div>
</div>


<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">ноябрь 2013 - май 2014</span>
        </div>
        <a href="photogallery/news/ivanteevka.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/ivanteevka_m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Выполнено проектирование и строительство завода по производству товарного бетона в г. Ивантеевка Московской области.<br>
            Работы по выемке грунта и бетонированию выполнялись в зимнее время. Монтаж и наладка оборудования в апреле-мае.
        </p>
    </div>
</div>


<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">январь - март 2014</span>
        </div>
        <a href="photogallery/news/styazka.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/styazka.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Снят и смонтирован обучающий ролик по выполнению полусухой стяжки по вспененному пенополистиролу (ППС) совместно с Ильёй Тарлевым (компания Экспресспол).<br>
            Адрес ролика на ютубе: <a href="http://www.youtube.com/watch?v=Jtw7-0XAyQo" target="_blank">http://www.youtube.com/watch?v=Jtw7-0XAyQo</a>
        </p>
    </div>
</div>




<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">декабрь 2013 - февраль 2014</span>
        </div>
        <a href="photogallery/news/lodzi.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/lodzi.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Снят и смонтирован обучающий ролик по утеплению и отделке лоджии с помощью вспененного пенополистирола  по технологии фасадной отделки.<br>
            Адрес ролика на ютубе: <a href="http://www.youtube.com/watch?v=XHEo5R1BKHg" target="_blank">http://www.youtube.com/watch?v=XHEo5R1BKHg</a>
        </p>
    </div>
</div>



<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">июль-август 2013</span>
        </div>
        <a href="photogallery/news/zelogor.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/zelogor_m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Сделано утепление фасада в г. Зеленогорске Ленинградской области<br>
            Теплоизоляция - минеральная вата &quot;Paroc&quot;, система BAUMIT, декоративная штукатурка - силиконовая.
        </p>
    </div>
</div>






<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">июнь-июль 2013</span>
        </div>
        <a href="photogallery/news/novriga.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/novriga.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Сделано утепление фасада в Истринском районе Московской области<br>
            Теплоизоляция - минеральная вата &quot;Paroc&quot;, система CERESIT, декоративная штукатурка - силикатная.<br>
        </p>
    </div>
</div>



<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">июль 2013 -  март 2014</span>
        </div>
        <a href="photogallery/news/vatutinki.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/vatutinki.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Сделано утепление фасада с украшением декоративными элементами в пос. Ватутинки, Московская область.<br>
            Теплоизоляция - пенополистирол, система KREISEL, декоративная штукатурка - силиконовая.<br>
            Зимой выполнена часть отделочных работ.<br>
            Сделана разводка систем отопления и водоснабжения медными трубами.
        </p>
    </div>
</div>




<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">июнь-июль 2013</span>
        </div>
        <a href="photogallery/news/345.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/345m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Сделано утепление фасада в Домодедовском районе Московской области<br>
            Теплоизоляция - минеральная вата &quot;Paroc&quot;, система BAUMIT, декоративная штукатурка - силиконовая.
        </p>
    </div>
</div>



<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">май - июнь 2013</span>
        </div>
        <a href="photogallery/news/dmd1-11.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/dmd1-11m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Сделано утепление фасада с украшением декоративными элементами в г. Домодедово, Московская область.<br>
            Теплоизоляция - пенополистирол, система STO, декоративная штукатурка - силиконовая.
        </p>
    </div>
</div>



<div class="col-sm-12 news-block">
    <hr>
    <div class="col-sm-4">
        <div class="col-sm-12">
            <span class="text-v10b">октябрь - 2012 август 2013</span>
        </div>
        <a href="photogallery/news/smolensk0.jpg" rel="lightbox">
            <img class="fotofon" src="photogallery/news/smolensk0_m.jpg">
        </a>
    </div>
    <div class="col-sm-8">
        <p>Выполнено проектирование, сделан фундамент и построена коробка коттеджа в Смоленской области. <br>
            Заливка бетонного фундамента производилась уже при отрицательных температурах в декабре.
        </p>
    </div>
</div>
<br>
<br>
<?php require 'footer.php'; ?>

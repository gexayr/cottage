<?php
ini_set("display_errors",1);
error_reporting(E_ALL);
function debug($array){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}
function debug_die($array){
    echo "<pre>";
    print_r($array);
    exit;
}
function dd($array){
    var_dump($array);
    exit;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Утепление-коттеджа.рф</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">

<!--    bootstrap-->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <script type="text/javascript" src="/litebox.js"></script>
    <link rel="stylesheet" type="text/css" href="/style/litebox.css">
    <link rel="stylesheet" type="text/css" href="/style/styles.css">
    <script>
        $(document).ready(function(){
            $("#navbar_button").click(function(){
                $("#second_level_menu").fadeToggle();
            });
        });
    </script>
</head>
<body>
<div class="container">
    <div class="row">
            <a href="/">
                <img src="img/logo.jpg" alt="Утепление коттеджа - утепление-коттеджа.рф"
                             class="img-responsive respons" height="63" border="0">
            </a>
        <div class="col-sm-12">
            <div class="col-md-3" align="center" valign="middle">
                <img src="img/pic01.jpg" width="187" height="140">
                <p class="spis2">- проектирование<br>
                    - общестроительные работы и отделка<br>
                    - монтаж инженерных систем</p>
            </div>
            <div class="col-md-3" align="center" valign="middle">
                <img src="img/pic02.jpg" width="183" height="140">
                <p class="spis2">- утепление фасадов зданий любой сложности и этажности<br>
                    - изготовление декоративных элементов</p>
            </div>
            <div class="col-md-3" align="center" valign="middle">
                <img src="img/pic03.jpg" width="172" height="140">
                <p class="spis2">- разработка технической документации<br>
                    - видеосъёмка строительных технологий<br>
                    - помощь в сертификации</p>
            </div>

            <div class="col-md-3" align="left" valign="middle">
                <div class="col-sm-12">РЕКОМЕНДУЕМ ПОСЕТИТЬ :</div>
                <div class="col-sm-12">
                    <a href="http://www.wdvs.ru" target="_blank" class="res2">
                        <img src="img/res1b.gif" width="90" height="33" hspace="2" border="0" align="left">
                        <p>Персональный информационный портал Г.Емeльянoва. Максимум информации о &quot;мокром фасаде&quot;
                        (СФТК), статьи и расчёты по различным строительным технологиям.</p>
                    </a>
                </div>
                <br>
                <br>
                <div class="col-sm-12">
                    <a href="http://hochusebedom.ru" target="_blank" class="res2">
                        <img src="img/res2b.gif" width="90" height="38" hspace="2" align="left">
                        <p>Строительный форум для общения. Если вам необходимо что-то узнать,
                        задайте вопрос, вам ответят специалисты и разбирающиеся в интересующейся теме посетители.</p>
                    </a>
                </div>

            </div>
        </div>
    </div>
<?php require 'menu.php'?>
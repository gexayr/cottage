<?php require 'header_home.php'; ?>
<div class="col-sm-12">
    <div class="row">
        <div align="center" class="red col-sm-12">Добро пожаловать! Спасибо, что посетили наш сайт!<br>
          Чем можем вам помочь?
        <hr>
        </div>

        <div class="col-sm-12">
            <div class="col-md-5">
                <img class="img-responsive" src="img/bulle_bubble.gif">
            </div>
            <div class="col-md-7">
                <div class="text-V12b home_1_text col-sm-12">
                    <div class="col-sm-1">
                        <img src="img/ar12.gif">
                    </div>
                    <span class="col-sm-11">Вы собираетесь строить коттедж, хотите разобраться в технологиях, с чего начинать,
                        какие есть нюансы. Вы просто ищете пока информацию. </span>
                </div>
                Можем вам предложить почитать инфопортал о строительстве - <a href="http://www.wdvs.ru" target="_blank">www.wdvs.ru</a>,
                где есть интересная информация по некоторым аспектам строительства:<br>
                - <a href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/gennady-montaz-mednih-trub.html"
                     target="_blank">монтаж медных труб на системы водоснабжения и отопления</a><br>
                - <a href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/gennady-polusuhya-styazka.html">технология
                    изготовления полусухой стяжки</a><br>
                -
                <a href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/gennady-osobennosti-raboti-s-gipsokartonom.html"
                   target="_blank">некоторые приёмы работы с гипсокартоном</a> и <a
                        href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/zadelka-shvov-gipsokartona.html"
                        target="_blank">заделка швов ГКЛ</a><br>
                - <a href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/kakoi-tolshini-uteplitel.html"
                     target="_blank">какой толщины должен быть утеплитель</a><br>
                - <a href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/gazobeton2.html" target="_blank">наиболее
                    полная информация о газобетонных блоках (газосиликате)</a><br>
                -
                <a href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/ostorozno-penopolistirol-v-stroitelstve.html"
                   target="_blank">информация о вспененном пенополистироле (EPS/ППС) </a><br>
                -
                <a href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/primenenie-extrudirovannogo-penopolistirola-v-stroitelstve.html"
                   target="_blank">где применяется экструдированный пенополистирол (XPS/ЭППС)</a><br>
                ...и другие статьи
            </div>
        </div>

        <div class="col-sm-12">
            <hr>
            <div class="col-md-7 home_second">
                <div class="text-V12b home_1_text col-sm-12">
                    <div class="col-sm-1">
                        <img src="img/ar12.gif">
                    </div>
                    <span class="col-sm-11">Вы собираетесь делать фасад и ищете информацию, как
                        правильно это сделать, чтобы можно было контролировать рабочих и вообще разобраться
                        хотя бы немного в технолологии фасадного утепления СФТК (&quot;мокрый фасад&quot;)
                    </span>
                </div>
                <span class="text-V12">
                    <p>Советуем вам сначала у нас прочитать страницу &quot;
                        <a href="fasad5.php">Важная информация для заказчика</a>&quot;
                        о некоторых первоочередных нюансах фасадных работ.
                    Какая ситуация с исполнителями работ, что вас может ожидать, на что обратить внимание, чтобы
                    избежать некоторых неприятностей. </p>
                        <p>Специально для всех мы создали полнометражный фильм по технологии теплоизоляции фасадов,
                            который очень рекомендуем вам посмотреть -&gt;<br>
                            Ещё много информации по технологии утепления штукатурных фасадов есть на инфопортале
                            <a href="http://www.wdvs.ru" target="_blank">www.wdvs.ru</a></p></span>
            </div>
            <div class="col-md-5">
                <div class="text-V12 second_home">
                    <iframe src="https://www.youtube.com/embed/p4GY5gXB3Yo?rel=0"
                            frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <hr>
            <div class="col-md-5">
                <img class="img-responsive" src="img/kladka.jpg">
            </div>
            <div class="col-md-7 home_third">
                <div class="col-sm-12 home_1_text text-V12b">
                    <div class="col-sm-1">
                        <img src="img/ar12.gif">
                    </div>
                    <span class="col-sm-11">Вы ищете, кто вам сможет спроектировать и построить коттедж? </span>
                </div>
            <span class="text-V12">Загляните в разделы &quot;<a href="proekt.php">Проектирование</a>&quot; и  &quot;
                <a href="stroitelstvo.php">Строительство</a>&quot;.
                <br>
                <br>
                В разделе &quot;Проектирование&quot;  вы можете изучить наши советы по проектированию, для чего это нужно или что
                вас ждёт при покупке готовых или скачивании бесплатных проектов, а также при &quot;проектировании&quot; с помощью вопросов на форумах.<br>
                <br>
                Мы проектируем, строим коттеджи и любые другие объекты.
                <br>
            </span>
            </div>
        </div>

        <div class="col-sm-12">
            <hr>
            <div class="col-md-7 home_third">
                <div class="text-V12b home_1_text col-sm-12">
                    <div class="col-sm-1">
                        <img src="img/ar12.gif">
                    </div>
                    <span class="col-sm-11" valign="middle">Вы ищете, кто вам выполнит фасадные работы?</span>
                </div>
                <span class="text-V12">
                    <p>Мы выполним. Не будем, как другие, писать пафосные слова
                          &quot;качественно, быстро, дёшево&quot;. В качестве и высоком уровне знаний и опыта вы можете
                          убедиться сами, изучив страницы в разделе &quot;<a href="fasad.php">Фасадные работы</a>&quot;,
                          высокое качество не может быть дешёвым, а &quot;быстро&quot; - это понятие относительное и зависит
                          от многих факторов.</p>
                    <p><a href="fasad3.php">- Почему мы лучше других</a><br>
                    <a href="fasad4.php">- Стоимость материала и работы по &quot;мокрому фасаду&quot;</a><br>
                    <a href="fasad2.php">- Наши некоторые сделанные объекты</a><br>
                    <a href="fasad5.php">- Важная информация для заказчика, который планирует фасадные работы</a></p>
                </span>
            </div>

            <div class="col-md-5">
                <img class="img-responsive" src="img/fasad.jpg">
            </div>

        </div>

          <span class="tags">газобетон газосиликат купить блоки купить пенобетон пеноблоки где найти рабочих на фасад
              готовые проекты купить цена строительные работы расценки за работу отделка отделочные работы как построить
              коттедж выполняем фасадные работы делаем утепление коттеджа утепляем фасад теплоизоляция пенопласт
              пенополистирол стекловата каменная вата минвата где купить клей выбрать обои медные трубы цена
              металлопластиковые трубы полипропиленовые пластиковые сделать трубы спаять рабочие на фасад какой толщины
              утепление какой толщины кирпичная стена из газоблоков из гасиликатных пеноблоков какой толщины утеплитель
              найти строителей фасадчиков фасадные работы термокраска стоимость фасадных работ компания выполняющая
              строительные отделочные фасадные работы украсить фасад изготовление декорэлементов декоративных элементов
              купить готовые декоры на фасад декоры из пенополистирола полиуретана напыление пенополиуретаном ППУ кладка
              блоков цена клея для кладки газосиликатных пеноблоков где купить плиточный клей обои фанеру кирпич цена
              под ключ строительства квартирный ремонт как утеплить лоджию как утеплить стены почему мокнут стены углы
              как где найти рабочих на фасад сделать отделку фасада утеплить проект каркасного дома бесплатный проект
              готовый проект купить каркасник брусовый дом чем утеплить стену как построить каркасный дом чем отделать
              фасад брусового коттеджа горючесть горит утеплитель пенопласт пенополистирол полиуретан пеноизол СИП
              панели кирпичная кладка поротерм поризованная керамика кирпичная кладка цена стоимость где купить цены
              построить коттедж купить земельный участок
          </span>

    </div>

</div>
<?php require 'footer.php'; ?>
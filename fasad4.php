<?php require 'header.php'; ?>
<h3 align="center">Стоимость материалов и фасадных работ</h3>


<div class="col-sm-12">
    <p class="font12">Стоимость выполнения всего комплекса фасадных работ (без материала) дана из расчёта на 1 кв.м. фасада, работа состоит из:<br>
        - подготовка основания, грунтовка<br>
        - приклеивание теплоизоляции под уровень<br>
        - дюбелирование из расчёта 5-6 дюбелей на 1 кв.м.<br>
        -
        обработка примыканий, проёмов, установка необходимых профилей<br>
        -
        создание базового клееармирующего слоя<br>
        -
        грунтовка и нанесение финишного колерованного в массе декоративного слоя<br>
        <br />
        В стоимость также входит погрузка-разгрузка фасадного материала; установка, снятие, погрузка-разгрузка строительных лесов.
        <strong>То есть весь комплекс работ, связанный с утеплением и отделкой фасада.</strong></p>
</div>


<h3 align="center"><strong>Стоимость нашей работы (без учёта материала)</strong></h3>
<div class="col-md-10 col-md-offset-1 price-table">
    <table border="1" align="center" cellpadding="5" cellspacing="0">
        <tr>
            <td colspan="3" valign="top" bgcolor="#E4E4E4" class="text-V12">
                <div align="center"><em><strong>варианты по стоимости:</strong></em></div>
            </td>
        </tr>
        <tr>
            <td width="30" align="center" valign="top" class="text-V12"><strong>1</strong></td>
            <td valign="top" class="text-V12">На объект доставляется оборудование, инструмент, бригада рабочих из числа тех, кто умеет выполнять фасадные работы. <br>
                Мы объект постоянно не контролируем, выезжаем на технический надзор 2-3 раза. Контроль работ - на заказчике.<br>
                Сроки начала и окончания работ по факту наличия свободных рабочих, где приоритет отдаётся другим вариантам работы.<br>
                (<em>Класс качества работы - &quot;обычная штукатурка&quot; по СНиП 3.04.01-87</em>).</td>
            <td valign="top" class="text-V12">от 1000 руб</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="text-V12"><strong>2</strong></td>
            <td valign="top" class="text-V12">На объект доставляется оборудование, инструмент, бригада наших постоянных рабочих-фасадчиков.<br>
                Объект контролируем периодически. Полный контроль работ - на заказчике.<br>
                Сроки начала и окончания работ по факту наличия свободных рабочих, где приоритет отдаётся другим вариантам работы.<br>
                (<em>Класс качества работы - &quot;обычная штукатурка&quot; по СНиП 3.04.01-87</em>).</td>
            <td width="100" valign="top" class="text-V12">от 1250 руб.</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="text-V12"><strong>3</strong></td>
            <td valign="top" class="text-V12">На объект доставляется оборудование, инструмент, бригада наших постоянных рабочих-фасадчиков.<br>
                Объект контролируем постоянно и несём полную ответственность за проведение работ и гарантийные обязательства.<br>
                (<em>Класс качества работы - &quot;улучшенная штукатурка&quot; по СНиП 3.04.01-87</em>).</td>
            <td width="100" valign="top" class="text-V12">от 1500 руб</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="text-V12"><strong>4</strong></td>
            <td height="60" valign="top" class="text-V12">Технический надзор за проведением фасадных работ на объекте заказчика не нашими силами.<br>
                осмотр, фото/видеофиксация этапа работ, переговоры с рабочими, заказчиками. <br>
                На основе анализа выполнения работ дополнительные письменные замечания/инструкции заказчику.<br>
                При этом на объекте мы не занимаемся обучением привлечённых для выполнения работ строителей. </td>
            <td valign="top" class="text-v10">от 8 тыс.руб. за выезд</td>
        </tr>
        <tr>
            <td colspan="3" valign="top" bgcolor="#E4E4E4" class="text-V12"><div align="center"><span class="font10-b"><em><strong>дополнительная оплата за:</strong></em></span></div></td>
        </tr>
        <tr>
            <td valign="top" class="text-V12">&nbsp;</td>
            <td valign="top" class="text-V12">Аренда, амортизация строительных лесов и оборудования</td>
            <td valign="top" class="text-V12">200-250 руб.</td>
        </tr>
        <tr>
            <td valign="top" class="text-V12">&nbsp;</td>
            <td valign="top" class="text-V12">Установка отливов, в зависимости от сложности установки (простой или улучшенный вариант)</td>
            <td valign="top" class="text-v10">в зависимости от сложности и объёмов</td>
        </tr>
        <tr>
            <td colspan="3" valign="top" bgcolor="#E4E4E4" class="text-V12"><div align="center" class="font10-b"><em><strong>возможные дополнительные расходы:</strong></em></div></td>
        </tr>
        <tr>
            <td valign="top" class="text-V12">&nbsp;</td>
            <td valign="top" class="text-V12">Компенсация кривизны стен которая вышла за пределы норм СНиП</td>
            <td valign="top" class="text-V12"><span class="text-v10">в зависимости от сложности</span></td>
        </tr>
        <tr>
            <td valign="top" class="text-V12">&nbsp;</td>
            <td valign="top" class="text-V12">Комплекс инженерных и технологических решений для повышения долговечности фасадной отделки</td>
            <td width="100" valign="top" class="text-V12">от 100 руб</td>
        </tr>
        <tr>
            <td valign="top" class="text-V12">&nbsp;</td>
            <td valign="top" class="text-V12">Дополнительная окраска декоративной штукатурки (в случае, если по каким-то соображениям на объекте применяется штукатурка не колерованная в массе с завода)</td>
            <td valign="top" class="text-V12">150 руб</td>
        </tr>
        <tr>
            <td valign="top" class="text-V12">&nbsp;</td>
            <td valign="top" class="text-V12">Усложнение выполнения работ (например врезка теплоизоляции в стропильную систему</td>
            <td valign="top" class="text-v10">в зависимости от сложности</td>
        </tr>
        <tr>
            <td valign="top" class="text-V12">&nbsp;</td>
            <td valign="top" class="text-V12">Транспортные расходы на привоз и вывоз строительных лесов и оборудования</td>
            <td width="100" valign="top" class="text-v10">в зависимости от расстояния</td>
        </tr>
        <tr>
            <td valign="top" class="text-V12">&nbsp;</td>
            <td valign="top" class="text-V12">Изготовление декоров и различных украшений, дизайн-решения<br /></td>
            <td width="100" valign="top" class="text-v10">в зависимости от сложности и объёмов</td>
        </tr>
    </table>

</div>



<div class="col-sm-12 mt-50">
    <span><strong>МЫ НЕ СМОЖЕМ ДАТЬ САМУЮ ДЕШЁВУЮ ЦЕНУ</strong>, так как не хотим экономить на качестве, инструменте, рабочих, их быте и безопасности. <br>
          Наши услуги, опыт и профессионализм не стоят дешевле других. <br>
          И главное - <strong>КАКУЮ ДЕШЁВУЮ ЦЕНУ КТО НЕ ДАСТ, ВСЕГДА МОЖНО НАЙТИ ЕЩЁ ДЕШЕВЛЕ</strong>.</span>
</div>

    <div class="col-sm-12">
        <p>Мы работаем на следующих условиях:</p>
        <p>- на объекте уже должна быть кровля или окна или, это лучше - стропильная система уже сделана, а кровельное покрытие и окна будут монтироваться одновременно с фасадными работами<br>
            - перед началом работ должны быть решён окончательный вид фасада, цветовые и фактурные решения<br>
            - в момент начала работ весь материал должен быть на объекте или есть материал на начало работ, но остальное будет привезено в ближайшее время и не создаст простоев в работе<br>
            - так как мы не хотим под увеличение объёмов случайных людей, работа выполняется проверенными, обученными людьми, то график начала и окончания работ необходимо согласовывать заблаговременно и по ситуации<br>
            - на объекте должен быть обеспечен быт для рабочих (бытовка для проживани в командировках), душ, электричество, вода<br>
            - при подсчёте объёма выполненных работ, количество сделанных квадратных метров считается по базовому или финишному слою, вычитаются оконные проёмы, но не вычитаются технологические отверстия, площадь откосов засчитывается с коэффициентом 2<br>
            - оплата работы по согласованному графику оплаты работ<br>
            - гарантийные обязательства даются стандартно на срок 2 года (но можно больше)<br>
            - мы не работаем со сколоченных деревянных лесов, вышек, люлек и лестниц, мы работаем только с инвентарных фасадных лесов</p>
    </div>

<div class="col-sm-12">
    <h3 align="center"><strong>Цена на материал из расчёта на 1 кв.м. фасада</strong>:<br />
        <span class="text-v10"><em>(кликнув на ссылку можно посмотреть изображение)</em></span></h3>
    <p><br />
        <strong>На данный момент в связи с непонятной ситуацией на рынке, в финансовом секторе цены могут быть другими.</strong> Стоимость даётся под конкретный объект. Скидки есть, хорошие.<br />
    </p>
    <p><span class="font10"><strong>!!!</strong> расход клеевых смесей реальный, а не рекламный, почему так - читайте раздел &quot;Важная информация заказчику&quot;.</span></p>
</div>

<div class="col-md-10 col-md-offset-1">
    <img src="photogallery/price/logoknauf.jpg">
    <br>
    <span class="text-V12blue">
        <strong class="font12-b">&quot;Эконом&quot;-вариант.</strong>
        <br/>
        Пенополистирол 100 мм, профили только угловые, минеральная декорштукатурка 3 мм белого цвета.
    </span>
</div>
<div class="col-md-10 col-md-offset-1 price-table">
    <table border="1" align="center" cellpadding="3" cellspacing="0">
        <tr>
            <td bgcolor="#E4E4E4" class="text-V12"><div align="center">наименование материала</div></td>
            <td width="70" class="text-V12"><div align="center">единица</div></td>
            <td width="70" class="text-V12"><div align="center">стоим.ед.</div></td>
            <td width="70" class="text-V12"><div align="center">расход</div></td>
            <td width="70" class="text-V12"><div align="center">стоимость</div></td>
        </tr>
        <tr>
            <td class="text-V12">Клей для приклейки EPS <a href="photogallery/price/knauf-sevener.jpg" rel="lightbox">KNAUF SEVENER</a></td>
            <td width="70" class="text-V12"><div align="center">кг</div></td>
            <td width="70" class="text-V12"><div align="center">17,5</div></td>
            <td width="70" class="text-V12"><div align="center">9</div></td>
            <td width="70" class="text-V12"><div align="center">157,90</div></td>
        </tr>
        <tr>
            <td class="text-V12">Пенополистирол фасадный толщина 100 мм</td>
            <td width="70" class="text-V12"><div align="center">куб.м.</div></td>
            <td width="70" class="text-V12"><div align="center">3100</div></td>
            <td width="70" class="text-V12"><div align="center">0,11</div></td>
            <td width="70" class="text-V12"><div align="center">341</div></td>
        </tr>
        <tr>
            <td class="text-V12">Дюбель с термоголовкой <a href="photogallery/price/koelner.jpg" rel="lightbox">KOELNER KI-170/8M</a></td>
            <td width="70" class="text-V12"><div align="center">шт</div></td>
            <td width="70" class="text-V12"><div align="center">5,56</div></td>
            <td width="70" class="text-V12"><div align="center">6</div></td>
            <td width="70" class="text-V12"><div align="center">33,36</div></td>
        </tr>
        <tr>
            <td class="text-V12">Стеклосетка фасадная щелочестойкая КРЕПИКС Фасад 160 г</td>
            <td width="70" class="text-V12"><div align="center">кв.м.</div></td>
            <td width="70" class="text-V12"><div align="center">27</div></td>
            <td width="70" class="text-V12"><div align="center">1,3</div></td>
            <td width="70" class="text-V12"><div align="center">35,1</div></td>
        </tr>
        <tr>
            <td class="text-V12"><a href="photogallery/price/ugolok.jpg" rel="lightbox">Уголок наружный PVC</a> с сеткой 15х10</td>
            <td width="70" class="text-V12"><div align="center">пог.м.</div></td>
            <td width="70" class="text-V12"><div align="center">20,5</div></td>
            <td width="70" class="text-V12"><div align="center">0,5</div></td>
            <td width="70" class="text-V12"><div align="center">10,25</div></td>
        </tr>
        <tr>
            <td class="text-V12">Клей для базового слоя <a href="photogallery/price/knauf-sevener.jpg" rel="lightbox">KNAUF SEVENER</a></td>
            <td width="70" class="text-V12"><div align="center">кг</div></td>
            <td width="70" class="text-V12"><div align="center">17,5</div></td>
            <td width="70" class="text-V12"><div align="center">6</div></td>
            <td width="70" class="text-V12"><div align="center">87,5</div></td>
        </tr>
        <tr>
            <td class="text-V12"><a href="photogallery/price/ms.jpg" rel="lightbox">Герметик MS-полимерный</a> для герметизации примыканий</td>
            <td width="70" class="text-V12"><div align="center">туба</div></td>
            <td width="70" class="text-V12"><div align="center">180</div></td>
            <td width="70" class="text-V12"><div align="center">0,05</div></td>
            <td width="70" class="text-V12"><div align="center">9</div></td>
        </tr>
        <tr>
            <td class="text-V12"><a href="photogallery/price/makroflex.jpg" rel="lightbox">Монтажная пена полиуретановая</a> для герметизации швов и зазоров</td>
            <td width="70" class="text-V12"><div align="center">баллон</div></td>
            <td width="70" class="text-V12"><div align="center">210</div></td>
            <td width="70" class="text-V12"><div align="center">0,01</div></td>
            <td width="70" class="text-V12"><div align="center">2,01</div></td>
        </tr>
        <tr>
            <td class="text-V12">Грунтовка базового слоя под декорштукатурку <a href="photogallery/price/knauf_isogrunt.jpg" rel="lightbox">КНАУФ Изогрунд</a></td>
            <td class="text-V12"><div align="center">кг</div></td>
            <td class="text-V12"><div align="center">69</div></td>
            <td class="text-V12"><div align="center">0,25</div></td>
            <td class="text-V12"><div align="center">17,25</div></td>
        </tr>
        <tr>
            <td class="text-V12">Минеральная декорштукатурка <a href="photogallery/price/knauf-diamant.jpg" rel="lightbox">KNAUF Диамант (белый) 3 мм</a></td>
            <td width="70" class="text-V12"><div align="center">кг</div></td>
            <td width="70" class="text-V12"><div align="center">14</div></td>
            <td width="70" class="text-V12"><div align="center">3,8</div></td>
            <td width="70" class="text-V12"><div align="center">53,2</div></td>
        </tr>
        <tr>
            <td class="text-V12">Прочие материалы и расходные инструменты</td>
            <td width="70" class="text-V12"><div align="center"></div></td>
            <td width="70" class="text-V12"><div align="center"></div></td>
            <td width="70" class="text-V12"><div align="center"></div></td>
            <td width="70" class="text-V12"><div align="center">20</div></td>
        </tr>
        <tr>
            <td colspan="4" class="text-V12"><div align="right" class="font12-b">итого:</div></td>
            <td width="70" class="text-V12"><div align="center" class="font12-b">784,07</div></td>
        </tr>
    </table>
    <hr>
</div>


<div class="col-md-10 col-md-offset-1">
    <img src="photogallery/price/bolarslogo.jpg">
    <br>
    <span class="text-V12blue">
        <strong class="font12-b">&quot;Оптимал&quot;-вариант.</strong>
    </span>
    <br>
    <span class="text-V12blue">
        Пенополистирол 100 мм, профили только угловые, декорштукатурка 1,5 мм колерованная в массе в светлый цвет.
    </span>
</div>

<div class="col-md-10 col-md-offset-1 price-table">
      <table border="1" align="center" cellpadding="3" cellspacing="0">
        <tr>
          <td bgcolor="#E4E4E4" class="text-V12"><div align="center">наименование материала</div></td>
          <td width="70" class="text-V12"><div align="center">единица</div></td>
          <td width="70" class="text-V12"><div align="center">стоим.ед.</div></td>
          <td width="70" class="text-V12"><div align="center">расход</div></td>
          <td width="70" class="text-V12"><div align="center">стоимость</div></td>
        </tr>
        <tr>
          <td class="text-V12">Грунтовка <a href="photogallery/price/bolars-primer.jpg" rel="lightbox">BOLARS глубокого проникновения</a></td>
          <td class="text-V12"><div align="center">кг</div></td>
          <td class="text-V12"><div align="center">45,60</div></td>
          <td class="text-V12"><div align="center">0,2</div></td>
          <td class="text-V12"><div align="center">9,12</div></td>
        </tr>
        <tr>
          <td class="text-V12">Клей для приклейки EPS <a href="photogallery/price/bolars-titanbond.jpg" rel="lightbox">BOLARS Titanbond</a></td>
          <td width="70" class="text-V12"><div align="center">кг</div></td>
          <td width="70" class="text-V12"><div align="center">17,20</div></td>
          <td width="70" class="text-V12"><div align="center">9</div></td>
          <td width="70" class="text-V12"><div align="center">154,80</div></td>
        </tr>
        <tr>
          <td class="text-V12">Пенополистирол фасадный толщина 100 мм</td>
          <td width="70" class="text-V12"><div align="center">куб.м.</div></td>
          <td width="70" class="text-V12"><div align="center">3100</div></td>
          <td width="70" class="text-V12"><div align="center">0,11</div></td>
          <td width="70" class="text-V12"><div align="center">341</div></td>
        </tr>
        <tr>
          <td class="text-V12">Дюбель с термоголовкой <a href="photogallery/price/koelner.jpg" rel="lightbox">KOELNER KI-170/8M</a></td>
          <td width="70" class="text-V12"><div align="center">шт</div></td>
          <td width="70" class="text-V12"><div align="center">5,56</div></td>
          <td width="70" class="text-V12"><div align="center">6</div></td>
          <td width="70" class="text-V12"><div align="center">33,36</div></td>
        </tr>
        <tr>
          <td class="text-V12">Стеклосетка фасадная щелочестойкая <a href="photogallery/price/valmiera.jpg" rel="lightbox">Valmiera 165 г</a></td>
          <td width="70" class="text-V12"><div align="center">кв.м.</div></td>
          <td width="70" class="text-V12"><div align="center">36,50</div></td>
          <td width="70" class="text-V12"><div align="center">1,3</div></td>
          <td width="70" class="text-V12"><div align="center">47,44</div></td>
        </tr>
        <tr>
          <td class="text-V12"><a href="photogallery/price/ugolok.jpg" rel="lightbox">Уголок наружный PVC</a> с сеткой 15х10</td>
          <td width="70" class="text-V12"><div align="center">пог.м.</div></td>
          <td width="70" class="text-V12"><div align="center">20,50</div></td>
          <td width="70" class="text-V12"><div align="center">0,6</div></td>
          <td width="70" class="text-V12"><div align="center">12,30</div></td>
        </tr>
        <tr>
          <td class="text-V12">Клей для базового слоя <a href="photogallery/price/bolars-armibond.jpg" rel="lightbox">BOLARS Armibond</a></td>
          <td width="70" class="text-V12"><div align="center">кг</div></td>
          <td width="70" class="text-V12"><div align="center">22,30</div></td>
          <td width="70" class="text-V12"><div align="center">6</div></td>
          <td width="70" class="text-V12"><div align="center">133,80</div></td>
        </tr>
        <tr>
          <td class="text-V12"><a href="photogallery/price/ms.jpg" rel="lightbox">Герметик MS-полимерный</a> для герметизации примыканий</td>
          <td width="70" class="text-V12"><div align="center">туба</div></td>
          <td width="70" class="text-V12"><div align="center">180</div></td>
          <td width="70" class="text-V12"><div align="center">0,03</div></td>
          <td width="70" class="text-V12"><div align="center">5,4</div></td>
        </tr>
        <tr>
          <td class="text-V12"><a href="photogallery/price/makroflex.jpg" rel="lightbox">Монтажная пена полиуретановая</a> для герметизации швов и зазоров</td>
          <td width="70" class="text-V12"><div align="center">баллон</div></td>
          <td width="70" class="text-V12"><div align="center">210</div></td>
          <td width="70" class="text-V12"><div align="center">0,01</div></td>
          <td width="70" class="text-V12"><div align="center">2,01</div></td>
        </tr>
        <tr>
          <td class="text-V12">Грунт  для базового слоя <a href="photogallery/price/bolars-acryl-primer.jpg" rel="lightbox">BOLARS Acryl-Primer QUARZ</a></td>
          <td class="text-V12"><div align="center">кг</div></td>
          <td class="text-V12"><div align="center">82,70</div></td>
          <td class="text-V12"><div align="center">0,25</div></td>
          <td class="text-V12"><div align="center">20,67</div></td>
        </tr>
        <tr>
          <td class="text-V12">Декорштукатурка <a href="photogallery/price/bolars-maldivas.jpg" rel="lightbox">BOLARS Maldivas K</a> 1,5 мм &quot;шуба&quot; колеров.св.тон</td>
          <td width="70" class="text-V12"><div align="center">кг</div></td>
          <td width="70" class="text-V12"><div align="center">75</div></td>
          <td width="70" class="text-V12"><div align="center">2,5</div></td>
          <td width="70" class="text-V12"><div align="center">187,5</div></td>
        </tr>
        <tr>
          <td class="text-V12">Прочие материалы и расходные инструменты</td>
          <td width="70" class="text-V12"><div align="center"></div></td>
          <td width="70" class="text-V12"><div align="center"></div></td>
          <td width="70" class="text-V12"><div align="center"></div></td>
          <td width="70" class="text-V12"><div align="center">30</div></td>
        </tr>
        <tr>
          <td colspan="4" class="text-V12"><div align="right" class="font12-b">итого:</div></td>
          <td width="70" class="text-V12"><div align="center">977,40</div></td>
        </tr>
      </table>
    <hr>
</div>


    <div class="col-md-10 col-md-offset-1">
        <img src="photogallery/price/baumit-logo.jpg">
        <br>
        <span class="text-V12blue">
            <strong class="font12-b">&quot;Премиум-XL&quot;-вариант (на пенополистироле)</strong>
        </span>
        <br>
        <span class="text-V12blue">
            Пенополистирол 120 мм, профили угловые и примыкания, силикатная декорштукатурка 2 мм колерованная в массе в светлый тон.
        </span>
    </div>
    <div class="col-md-10 col-md-offset-1 price-table">
        <table border="1" align="center" cellpadding="3" cellspacing="0">
            <tr>
                <td bgcolor="#E4E4E4" class="text-V12"><div align="center">наименование материала</div></td>
                <td width="70" class="text-V12"><div align="center">единица</div></td>
                <td width="70" class="text-V12"><div align="center">стоим.ед.</div></td>
                <td width="70" class="text-V12"><div align="center">расход</div></td>
                <td width="70" class="text-V12"><div align="center">стоимость</div></td>
            </tr>
            <tr>
                <td class="text-V12">Дополнительный обрызг бетонных элементов <a href="photogallery/price/baumit-vorspritzer.jpg" rel="lightbox">BAUMIT VorSpritzer 2 мм</a></td>
                <td class="text-V12"><div align="center">кг</div></td>
                <td class="text-V12"><div align="center">9</div></td>
                <td class="text-V12"><div align="center">1</div></td>
                <td class="text-V12"><div align="center">9</div></td>
            </tr>
            <tr>
                <td class="text-V12">Клей для приклейки EPS <a href="photogallery/price/baumit-starcontact.jpg" rel="lightbox">BAUMIT StarContact</a></td>
                <td width="70" class="text-V12"><div align="center">кг</div></td>
                <td width="70" class="text-V12"><div align="center">22</div></td>
                <td width="70" class="text-V12"><div align="center">9</div></td>
                <td width="70" class="text-V12"><div align="center">198</div></td>
            </tr>
            <tr>
                <td class="text-V12">Пенополистирол  фасадный толщина 120 мм</td>
                <td width="70" class="text-V12"><div align="center">куб.м.</div></td>
                <td width="70" class="text-V12"><div align="center">3100</div></td>
                <td width="70" class="text-V12"><div align="center">0,135</div></td>
                <td width="70" class="text-V12"><div align="center">418,50</div></td>
            </tr>
            <tr>
                <td class="text-V12">Дюбель с термоголовкой <a href="photogallery/price/ejot_tid.jpg" rel="lightbox">EJOT TID-T 8/60 L x 195</a></td>
                <td width="70" class="text-V12"><div align="center">шт</div></td>
                <td width="70" class="text-V12"><div align="center">12,61</div></td>
                <td width="70" class="text-V12"><div align="center">7</div></td>
                <td width="70" class="text-V12"><div align="center">75,66</div></td>
            </tr>
            <tr>
                <td class="text-V12">Стеклосетка фасадная щелочестойкая <a href="photogallery/price/valmiera.jpg" rel="lightbox">Valmiera 165 г</a></td>
                <td width="70" class="text-V12"><div align="center">кв.м.</div></td>
                <td width="70" class="text-V12"><div align="center">36,50</div></td>
                <td width="70" class="text-V12"><div align="center">1,3</div></td>
                <td width="70" class="text-V12"><div align="center">47,44</div></td>
            </tr>
            <tr>
                <td class="text-V12"><a href="photogallery/price/ugolok.jpg" rel="lightbox">Уголок наружный PVC</a> с сеткой 15х10</td>
                <td width="70" class="text-V12"><div align="center">пог.м.</div></td>
                <td width="70" class="text-V12"><div align="center">20,5</div></td>
                <td width="70" class="text-V12"><div align="center">0,8</div></td>
                <td width="70" class="text-V12"><div align="center">16,40</div></td>
            </tr>
            <tr>
                <td class="text-V12"><a href="photogallery/price/okonniy.jpg" rel="lightbox">Профиль примыкания оконный</a> самоклеящийся 6 мм с сеткой</td>
                <td class="text-V12"><div align="center">пог.м.</div></td>
                <td class="text-V12"><div align="center">50,40</div></td>
                <td class="text-V12"><div align="center">0,3</div></td>
                <td class="text-V12"><div align="center">15,12</div></td>
            </tr>
            <tr>
                <td class="text-V12">Клей для базового слоя <a href="photogallery/price/baumit-starcontact.jpg" rel="lightbox">BAUMIT StarContact</a></td>
                <td width="70" class="text-V12"><div align="center">кг</div></td>
                <td width="70" class="text-V12"><div align="center">22</div></td>
                <td width="70" class="text-V12"><div align="center">6</div></td>
                <td width="70" class="text-V12"><div align="center">132</div></td>
            </tr>
            <tr>
                <td class="text-V12"><a href="photogallery/price/ms.jpg" rel="lightbox">Герметик MS-полимерный</a> для герметизации примыканий</td>
                <td width="70" class="text-V12"><div align="center">туба</div></td>
                <td width="70" class="text-V12"><div align="center">180</div></td>
                <td width="70" class="text-V12"><div align="center">0,03</div></td>
                <td width="70" class="text-V12"><div align="center">5,40</div></td>
            </tr>
            <tr>
                <td class="text-V12"><a href="photogallery/price/ceresit-ct84.jpg" rel="lightbox">Клей монтажный CERESIT CT-84</a> для герметизации швов и зазоров</td>
                <td width="70" class="text-V12"><div align="center">баллон</div></td>
                <td width="70" class="text-V12"><div align="center">480</div></td>
                <td width="70" class="text-V12"><div align="center">0,03</div></td>
                <td width="70" class="text-V12"><div align="center">14,40</div></td>
            </tr>
            <tr>
                <td class="text-V12">Грунт для базового слоя под декор. штукат. <a href="photogallery/price/baumit-uniprimer.png" rel="lightbox">BAUMIT UniPrimer</a></td>
                <td class="text-V12"><div align="center">кг</div></td>
                <td class="text-V12"><div align="center">78</div></td>
                <td class="text-V12"><div align="center">0,25</div></td>
                <td class="text-V12"><div align="center">19,50</div></td>
            </tr>
            <tr>
                <td class="text-V12">Силикатная  дек. штук. 2 мм &quot;шуба&quot; колеров. <a href="photogallery/price/baumit-silikattop.jpg" rel="lightbox">BAUMIT SilikatTop</a></td>
                <td width="70" class="text-V12"><div align="center">кг</div></td>
                <td width="70" class="text-V12"><div align="center">116</div></td>
                <td width="70" class="text-V12"><div align="center">3,3</div></td>
                <td width="70" class="text-V12"><div align="center">382,80</div></td>
            </tr>
            <tr>
                <td class="text-V12">Прочие материалы и расходные инструменты</td>
                <td width="70" class="text-V12"><div align="center"></div></td>
                <td width="70" class="text-V12"><div align="center"></div></td>
                <td width="70" class="text-V12"><div align="center"></div></td>
                <td width="70" class="text-V12"><div align="center">60</div></td>
            </tr>
            <tr>
                <td colspan="4" class="text-V12"><div align="right" class="font12-b">итого:</div></td>
                <td width="70" class="text-V12"><div align="center" class="font12-b">1394,22</div></td>
            </tr>
        </table>
        <hr>
    </div>

    <div class="col-md-10 col-md-offset-1">
        <img src="photogallery/price/baumit-logo.jpg">
        <br>
        <span class="text-V12blue">
        <strong class="font12-b">
            &quot;Премиум-XXL&quot;-вариант (на минеральной каменной вате)
        </strong>
        </span>
            <br>
            <span class="text-V12blue">
            Минвата PAROC 120 мм, профили угловые и примыкания, силикатная декорштукатурка 2 мм колерованная в массе в светлый тон.
        </span>
    </div>
    <div class="col-md-10 col-md-offset-1 price-table">
        <table border="1" align="center" cellpadding="3" cellspacing="0">
            <tr>
                <td bgcolor="#E4E4E4" class="text-V12"><div align="center">наименование материала</div></td>
                <td width="70" class="text-V12"><div align="center">единица</div></td>
                <td width="70" class="text-V12"><div align="center">стоим.ед.</div></td>
                <td width="70" class="text-V12"><div align="center">расход</div></td>
                <td width="70" class="text-V12"><div align="center">стоимость</div></td>
            </tr>
            <tr>
                <td class="text-V12">Дополнительный обрызг бетонных элементов <a href="photogallery/price/baumit-vorspritzer.jpg" rel="lightbox">BAUMIT VorSpritzer 2 мм</a></td>
                <td class="text-V12"><div align="center">кг</div></td>
                <td class="text-V12"><div align="center">9</div></td>
                <td class="text-V12"><div align="center">1</div></td>
                <td class="text-V12"><div align="center">9</div></td>
            </tr>
            <tr>
                <td class="text-V12">Клей для приклейки минваты <a href="photogallery/price/baumit-procontact.jpg" rel="lightbox">BAUMIT ProContact</a></td>
                <td width="70" class="text-V12"><div align="center">кг</div></td>
                <td width="70" class="text-V12"><div align="center">19,2</div></td>
                <td width="70" class="text-V12"><div align="center">10</div></td>
                <td width="70" class="text-V12"><div align="center">192</div></td>
            </tr>
            <tr>
                <td class="text-V12">Минвата базальтовая фасадная PAROC FAS4 толщина 120 мм</td>
                <td width="70" class="text-V12"><div align="center">куб.м.</div></td>
                <td width="70" class="text-V12"><div align="center">5900</div></td>
                <td width="70" class="text-V12"><div align="center">0,135</div></td>
                <td width="70" class="text-V12"><div align="center">796,50</div></td>
            </tr>
            <tr>
                <td class="text-V12">Дюбель с термоголовкой <a href="photogallery/price/ejot_tid.jpg" rel="lightbox">EJOT TID-T 8/60 L x 195</a></td>
                <td width="70" class="text-V12"><div align="center">шт</div></td>
                <td width="70" class="text-V12"><div align="center">12,61</div></td>
                <td width="70" class="text-V12"><div align="center">7</div></td>
                <td width="70" class="text-V12"><div align="center">75,66</div></td>
            </tr>
            <tr>
                <td class="text-V12">Стеклосетка фасадная щелочестойкая <a href="photogallery/price/valmiera.jpg" rel="lightbox">Valmiera 165 г</a></td>
                <td width="70" class="text-V12"><div align="center">кв.м.</div></td>
                <td width="70" class="text-V12"><div align="center">36,50</div></td>
                <td width="70" class="text-V12"><div align="center">1,3</div></td>
                <td width="70" class="text-V12"><div align="center">47,44</div></td>
            </tr>
            <tr>
                <td class="text-V12"><a href="photogallery/price/ugolok.jpg" rel="lightbox">Уголок наружный PVC</a> с сеткой 15х10</td>
                <td width="70" class="text-V12"><div align="center">пог.м.</div></td>
                <td width="70" class="text-V12"><div align="center">20,5</div></td>
                <td width="70" class="text-V12"><div align="center">0,8</div></td>
                <td width="70" class="text-V12"><div align="center">16,40</div></td>
            </tr>
            <tr>
                <td class="text-V12"><a href="photogallery/price/okonniy.jpg" rel="lightbox">Профиль примыкания оконный</a> самоклеящийся 6 мм с сеткой</td>
                <td class="text-V12"><div align="center">пог.м.</div></td>
                <td class="text-V12"><div align="center">50,40</div></td>
                <td class="text-V12"><div align="center">0,3</div></td>
                <td class="text-V12"><div align="center">15,12</div></td>
            </tr>
            <tr>
                <td class="text-V12">Клей для базового слоя <a href="photogallery/price/baumit-procontact.jpg" rel="lightbox">BAUMIT ProContact</a></td>
                <td width="70" class="text-V12"><div align="center">кг</div></td>
                <td width="70" class="text-V12"><div align="center">19,2</div></td>
                <td width="70" class="text-V12"><div align="center">7</div></td>
                <td width="70" class="text-V12"><div align="center">134,40</div></td>
            </tr>
            <tr>
                <td class="text-V12"><a href="photogallery/price/ms.jpg" rel="lightbox">Герметик MS-полимерный</a> для герметизации примыканий</td>
                <td width="70" class="text-V12"><div align="center">туба</div></td>
                <td width="70" class="text-V12"><div align="center">180</div></td>
                <td width="70" class="text-V12"><div align="center">0,03</div></td>
                <td width="70" class="text-V12"><div align="center">5,40</div></td>
            </tr>
            <tr>
                <td class="text-V12"><a href="photogallery/price/ceresit-ct84.jpg" rel="lightbox">Клей монтажный CERESIT CT-84</a> для герметизации швов и зазоров</td>
                <td width="70" class="text-V12"><div align="center">баллон</div></td>
                <td width="70" class="text-V12"><div align="center">480</div></td>
                <td width="70" class="text-V12"><div align="center">0,03</div></td>
                <td width="70" class="text-V12"><div align="center">14,40</div></td>
            </tr>
            <tr>
                <td class="text-V12">Грунт для базового слоя под декор. штукат. <a href="photogallery/price/baumit-uniprimer.png" rel="lightbox">BAUMIT UniPrimer</a></td>
                <td class="text-V12"><div align="center">кг</div></td>
                <td class="text-V12"><div align="center">78</div></td>
                <td class="text-V12"><div align="center">0,25</div></td>
                <td class="text-V12"><div align="center">19,50</div></td>
            </tr>
            <tr>
                <td class="text-V12">Силикатная  дек. штук. 2 мм &quot;шуба&quot; колеров. <a href="photogallery/price/baumit-silikattop.jpg" rel="lightbox">BAUMIT SilikatTop</a></td>
                <td width="70" class="text-V12"><div align="center">кг</div></td>
                <td width="70" class="text-V12"><div align="center">116</div></td>
                <td width="70" class="text-V12"><div align="center">3,3</div></td>
                <td width="70" class="text-V12"><div align="center">382,80</div></td>
            </tr>
            <tr>
                <td class="text-V12">Прочие материалы и расходные инструменты</td>
                <td width="70" class="text-V12"><div align="center"></div></td>
                <td width="70" class="text-V12"><div align="center"></div></td>
                <td width="70" class="text-V12"><div align="center"></div></td>
                <td width="70" class="text-V12"><div align="center">60</div></td>
            </tr>
            <tr>
                <td colspan="4" class="text-V12"><div align="right" class="font12-b">итого:</div></td>
                <td width="70" class="text-V12"><div align="center" class="font12-b">1768,62</div></td>
            </tr>
        </table>
        <hr>
    </div>


<div class="col-sm-12">
    <p>Цены и расходы материалов приведены ближе к максимуму, чем минимально (что делается у многих для
        завлекательных красивых цифр), поэтому, если будете сравнивать - имейте это ввиду, что сначала вас
        заинтересуют недорогими ценами, а потом вы всё равно отдадите больше, потому что &quot;тут больше расход
        потому что...&quot;, &quot;вдруг выяснилось, что...&quot; и так будет до бесконечности. <br />
        Конкретная сумма всё равно зависит от сложности, архитектурных решений, отдельных пожеланий. <br />
        Цены даны &quot;розничные&quot; (как выше указно - ближе к максимальной цене) - скидки уточняются от конкретного объекта при
        осмотре, расчёте и подписании договора. <br>
        <br>
        Комплектация фасадной системы - это как конструктор, в котором можно применить различные компоненты, различные решения.
        Представленные здесь варианты комплектации являются очень усреднёнными и ориентировочными.</p>
    <p>Вы можете ориентируясь на представленные цены задать вашу примерную стоимость, которую вы ходите увидеть
        за 1 кв.м. и мы под эти условия можем подобрать комплект материалов. Правило в ценообразовании - чем выше цена, тем лучше материал.</p>
    <p>Мы можем работать с любым материалом, предоставленным заказчиком, но очень часто случается, что материал
        оказывается ненадлежащего качества (например, не выдержанный пенополистирол, дюбеля типа РАЙСТОКС),
        не оптимальной комплектации (например, профили без сетки или трёхсоставной бийский дюбель), имеет свои
        особенности (например, армированные волокна, необходимость смешивать компоненты), на что уходит
        дополнительное время во время монтажа, поэтому за материал ненадлежащей комплектации или качества, то
        возможно повышение цены за работу и отказ от гарантийных обязательств.</p>
</div>

    <div class="col-sm-12">
        <strong>Вам необходимо рассчитать стоимость работ на вашем объекте, составить подробную калькуляцию по материалам?</strong><br>
        <br>
        Да, мы это делаем. <br>
        Для расчётов нам нужно получить от вас проект.<br>
        Мы сделаем расчёт бесплатно  в случае, если вас устраивают наши цены на работу и мы будем выполнять фасадные работы.
        Если объём фасада более 300 кв.м. или отделка фасада сложная, стоимость расчёта определяется по взаимной договорённости.<br>
        Если  вам нужен только расчёт (без наших фасадных работ), то  стоимость составления сметы для небольшого частного
        коттеджа (до 300 кв.м. по фасаду) составит на основе проекта в &quot;автокаде&quot; 10 тыс.руб. Если в других форматах, то 12 тыс.руб.
    </div>

    <div class="col-sm-12 fasad-next" align="center"><a href="fasad5.php">&gt; на следующую страницу &quot;Важная информация заказчику&quot;</a></div>

<?php require 'footer.php'; ?>
<?php require 'header.php'; ?>
<h4 align="center">Чем наша работа лучше, чем у других</h4>

<div class="col-sm-12">
    <p class="fs-16">Уже достаточно много лет в России производится фасадное утепление, с середины 90-х годов эта технология была как некий эксклюзив и возможность владельцу здания отличиться, с середины 2000-х годов возникла уже экономическая необходимость в связи с постепенным ростом цены на  энергоносители. На данный момент  технология СФТК (&quot;мокрый фасад&quot;) стала массовой.<br />
        Но до сих пор мало кто её делает качественно.<br>
        Ни в каких учебных заведениях этому не учат. Потому обучение технологии - только на базе производителей в их интересах или кто хочет обучается самостоятельно, как сможет.<br>
        Уровень знаний по фасадным технологиям низкий вообще. По штукатурным фасадам не было и до сих пор нет государственного стандарта.<br>
        В фирмах-производителях фасадных систем хороших специалистов по фасадам практически нет, только в редких случаях.<br>
        Тем не менее работать надо, к тому же материал для фасадов есть и даже иногда очень хороший.<br>
        Вы можете сами попробовать изучить все тонкости технологии и контролировать процесс выполнения работ на объекте, а можете поискать специалистов. А вот тут понадобятся знания и удача.<br>
        <br>
        <strong>Если вы ищете подрядчиков на свой объект, примите во внимание то, чем мы можем оказаться лучше, чем другие, предлагающие на рынке свои услуги:</strong></p>
</div>

<div class="col-sm-12">
    <div class="col-sm-1">
        <img src="img/good1.png">
    </div>

    <span class="col-sm-11 fs-16">
    <span class="red">
Самое главное и важное событие
        на рынке exСССР   - выпуск полнометражного фильма по технологии фасадной теплоизоляции &quot;СФТК&quot; (&quot;мокрый фасад&quot;),
        пошаговая инструкция, как правильно выполнять работы.</span>
        <span class="">(<a href="http://www.wdvs.ru/opisanie-sistem-teploizolyatsii/facade-movie.html" target="_blank" class="text-v10"><em>Подробнее о производстве фильма</em></a>)</span></p>
        </span>
</div>


<div class="col-sm-12">
    <p class="fs-16">Фильм сделан в новом прогрессивном формате &quot;4К&quot;. Надо отметить, что ролик &quot;Изготовление декорэлементов на фасаде&quot;
        вышел в РФ в формате &quot;4К&quot; в феврале 2015 года.
        Киноиндустрия России <a href="http://www.deloru.ru/news/568749/" target="_blank">первый фильм в формате
            &quot;4К&quot;</a> выпустила в прокат в апреле 2015 г.
        (<a href="https://ru.wikipedia.org/wiki/%D0%A2%D0%B5%D1%80%D1%80%D0%B8%D1%82%D0%BE%D1%80%D0%B8%D1%8F_(%D1%84%D0%B8%D0%BB%D1%8C%D0%BC,_2014)"
            target="_blank">к/ф &quot;Территория&quot;</a>).<br>
    </p>
    <p class="fs-16">Фильм &quot;Технология СФТК&quot; был выпущен в мае 2015 года.<br>
    </p>
    <p class="fs-16"><span class="red">То есть мы, &quot;простая компания строителей&quot;... взяли и сняли полнометражный фильм в новом
            формате &quot;4К&quot;, практически одновременно с киностудиями.</span> С одним роликом (&quot;Декоры&quot;) даже опередили.</p>
    <p class="fs-16">Операторскую работу, постановку, монтаж фильма сделал Г.Емeльянов</p>
</div>
<div class="col-sm-12">
    <div class="col-md-6 col-md-offset-3 second_home">
        <iframe src="https://www.youtube.com/embed/p4GY5gXB3Yo?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
</div>
<div class="col-sm-12">
    <p class="fs-16">Фильм предназначен для широкого круга строителей, заказчиков, производителей систем с целью обучения технологии выполнения работ.</p>
</div>

<div class="col-sm-12 mt-50">
    <div class="col-sm-1">
        <img src="img/good.png">
    </div>
    <div class="col-sm-11">
        <p class="fs-16">Для различных компаний и организаций мы выпустили уже несколько роликов и продолжаем сотрудничать и производить:</p>
    </div>
</div>


<div class="col-sm-12 mt-50">
    <div class="col-md-6 second_home">
        <iframe src="https://www.youtube.com/embed/XHEo5R1BKHg?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-6 mt-50">
        <div class="col-sm-12" align="center">
            <div class="col-md-4">
                <a href="http://www.sibur.ru" target="_blank">
                    <img src="img/Sibur_2016.gif">
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://epsrussia.ru" target="_blank">
                    <img src="img/eps_logo_s.gif">
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://www.alphapor.ru" target="_blank">
                    <img src="img/alphapor1.gif">
                </a>
            </div>
            <div class="col-sm-12" align="left">
                <p class="fs-16">Для компании &quot;Сибур&quot; и &quot;Ассоциации Поставщиков и Производителей Пенополистирола&quot; сделан видеоролик<br>
                    &quot;Технология утепления лоджии&quot;</p>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 mt-50">
    <div class="col-md-6 second_home">
        <iframe src="https://www.youtube.com/embed/Jtw7-0XAyQo?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-6 mt-50">
        <div class="col-sm-12" align="center">
            <div class="col-md-4">
                <a href="http://www.sibur.ru" target="_blank">
                    <img src="img/Sibur_2016.gif">
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://epsrussia.ru" target="_blank">
                    <img src="img/eps_logo_s.gif">
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://www.alphapor.ru" target="_blank">
                    <img src="img/alphapor1.gif">
                </a>
            </div>
            <div class="col-sm-12" align="left">
                <p class="fs-16">Для компании &quot;Сибур&quot; и &quot;Ассоциации Поставщиков и Производителей Пенополистирола&quot; сделан видеоролик<br>
                    &quot;Технология изготовления полусухой стяжки&quot;</p>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 mt-50">
    <div class="col-md-6 second_home">
        <iframe src="https://www.youtube.com/embed/vumimOLpKXY?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-6 mt-50">
        <div class="col-sm-12" align="center">
            <div class="col-md-12">
                <a href="http://www.ceresit.ru" target="_blank">
                    <img src="img/logo-ceresit1.gif">
                </a>
            </div>
            <div class="col-sm-12" align="left">
                <p class="fs-16">Для компании CERESIT сделан обучающий ролик по технологии наливной стяжки с помощью смеси Ceresit СТ178</p>
            </div>
        </div>
    </div>
</div>



<div class="col-sm-12 mt-50">
    <div class="col-md-6 second_home">
        <iframe src="https://www.youtube.com/embed/D2ST8TEqCCQ?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-6 mt-50">
        <div class="col-sm-12" align="center">
            <div class="col-md-4">
                <a href="http://www.sibur.ru" target="_blank">
                    <img src="img/Sibur_2016.gif">
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://epsrussia.ru" target="_blank">
                    <img src="img/eps_logo_s.gif">
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://www.alphapor.ru" target="_blank">
                    <img src="img/alphapor1.gif">
                </a>
            </div>
            <div class="col-sm-12" align="left">
                <p class="fs-16">Для компании &quot;Сибур&quot; и &quot;Ассоциации Поставщиков и Производителей Пенополистирола&quot; сделан видеоролик<br>
                    &quot;Технология изготовления декорэлементов на фасаде здания&quot;</p>
            </div>
        </div>
    </div>
</div>


<div class="col-sm-12 mt-50">
    <div class="col-md-6 second_home">
        <iframe src="https://www.youtube.com/embed/KH65kD0omsg?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-6 mt-50">
        <div class="col-sm-12" align="center">
            <div class="col-md-12">
                <a href="http://www.ceresit.ru" target="_blank">
                    <img src="img/anfas1.gif">
                </a>
            </div>
            <div class="col-sm-12" align="left">
                <p class="fs-16">Для Ассоциации &quot;АНФАС&quot; (Ассоциации Производителей и Поставщиков Фасадных Систем) сделан первый ролик из серии &quot;Авторитетное мнение&quot;.</p>
            </div>
        </div>
    </div>
</div>




<div class="col-sm-12 mt-50">
    <div class="col-md-6 second_home">
        <iframe src="https://www.youtube.com/embed/SocctaXFRwY?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-6 mt-50">
        <div class="col-sm-12" align="center">
            <div class="col-md-4">
                <a href="http://www.sibur.ru" target="_blank">
                    <img src="img/Sibur_2016.gif">
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://epsrussia.ru" target="_blank">
                    <img src="img/eps_logo_s.gif">
                </a>
            </div>
            <div class="col-md-4">
                <a href="http://www.alphapor.ru" target="_blank">
                    <img src="img/alphapor1.gif">
                </a>
            </div>
            <div class="col-sm-12" align="left">
                <p class="fs-16">Для компании &quot;Сибур&quot; и &quot;Ассоциации Поставщиков и Производителей Пенополистирола&quot; сделан видеоролик<br>
                    &quot;Технология строительства энергоэффективных каркасных зданий&quot;</p>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <p class="fs-16"><a href="http://www.youtube.com/user/8115522" target="_blank">Подпишитесь на канал YOUTUBE</a>, чтобы увидеть новые видеоролики и видеофильмы в числе первых.</p>
</div>


<div class="col-sm-12 mt-50">
    <p class="fs-16"> <img src="img/good.png"> В качестве эксперта Г.Емeльянова приглашали на передачу канал &quot;РБК&quot;</p>
</div>



<div class="col-sm-12">
    <div class="col-md-6 col-md-offset-3 second_home">
        <iframe src="https://www.youtube.com/embed/vjwqm5SUX3g?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
</div>

<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png">
        У нас большой опыт фасадных работ. Занимаемся
        строительством и отделкой с 1994 г. а утеплением фасадов с 1997 года, когда мы специализировались только на
        утеплении фасадов и в первые три года выполнили большой объём фасадных работ (порядка 40 тысяч кв.м.), на сегодня
        это примерно 120 тыс.кв.м, но с больших объектов мы практически ушли, т.к. там главное - цена, а работать плохим
        материалом и конкурировать с дешёвыми предложениями, значит, делать плохо, нас не устраивает.</p>
</div>
<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png">
        Наши  рабочие-фасадчики хорошо зарабатывают, держатся
        за работу, а потому стараются делать хорошо. Мы обеспечиваем хорошим профессиональным инструментом, нормальными условиями труда.<br>
        Мы обучаемих работать качественно, обращая внимания на все важные мелочи и нюансы. Требуем неукоснительно соблюдать
        технологию производства работ. Многие начинали с разнорабочих, теперь очень хорошие мастера.</p>
</div>
<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png">
        Во многих фирмах руководители контроль за рабочими поручают самим рабочим и бригадирам (прорабам). У нас
        непосредственно руководитель осуществляет контроль выполняемых работ и требует от рабочих делать всё правильно
        и качественно даже то, что заказчик не заметит.</p>
</div>
<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png">
        Руководитель компании Г. Емeльянов знаком непосредственно со многими руководителями компаний, производящих
        материалы для фасадных систем. Это позволяет быть в курсе всех новостей, событий и возможные проблемы решать
        напрямую и быстро, получать самый качественный материал и иметь скидки на закупаемые материалы. На рынке очень
        много подделок, брака, &quot;в этот раз не очень хорошо получившейся партии&quot; и прочих нюансов, которые не знают
        зачастую даже дилеры, не говоря уже об обычных строительных фирмах и бригадах.<br>
        К примеру, в продаже зачастую сложно бывает найти хороший пенополистирол или минвату. Качество минваты сильно
        упало, потому что производители не могут поднимать цену. Пенополистирол очень часто свежий, который не годится
        на фасад и иногда продавцы лукавят с его плотностью (маркой).<br>
    </p>
</div>

<div class="col-md-8 col-md-offset-2 mt-50">
    <div class="col-sm-4">
        <a href="photogallery/other/eps1.jpg" rel="lightbox"><img class="mob-respons" src="photogallery/other/eps1m.jpg" border="0"></a>
    </div>
    <div class="col-sm-8">
        <span class="text-v10">Фасадный пенополистирол должен быть выдержан 14 дней по ГОСТу, но фактически 1 месяц на
            складе в кубах, только после этого идёт в резку на листы. <strong>Все</strong> производители в РФ это не
            соблюдают. Когда на объект поступают уже изогнутые листы пенополистирола, то  его вообще не держали ни одного
            дня, как это предписано ГОСТ и ТУ, но чаще его выдерживают несколько дней, поэтому сначала он ровный, но всё
            равно спустя 2-3 недели становится видно, как его изогнуло. Если листы сразу поклеить на стену, то со времнем
            всё равно пенополистирол гнётся, идёт усадка и  между листами появляются зазоры 2-3 мм. Всё это негативно
            отражается на качестве работ и долговечности отделки.</span>
    </div>
</div>

<div class="col-md-8 col-md-offset-2 mt-50">
    <div class="col-sm-4">
        <a href="photogallery/other/karniz.jpg" rel="lightbox"><img class="mob-respons" src="photogallery/other/karniz_m.jpg"></a>
    </div>
    <div class="col-sm-8">
        <span class="text-v10">Также происходит с покупкой готовых декоров. С завода привозят свежий пенополистирол,
            не выдерживают, сразу режут, после чего спустя некоторое время все декоры изводит и их трудно приклеить и отделать ровно.</span>
    </div>
</div>



<div class="col-md-8 col-md-offset-2 mt-50">
    <div class="col-sm-4">
        <a href="photogallery/other/eps2.jpg" rel="lightbox"><img class="mob-respons" src="photogallery/other/eps2m.jpg"></a>
    </div>
    <div class="col-sm-8">
        <span class="text-v10"><strong>Мы для своих заказчиков заказываем пенополистирол из лучшего сырья и завод для нас выдерживает пенополистирол сколько это надо.</strong><br />
                  <br />
                Для всех остальных все производители только говорят, что всё выдержано, всё замечательно.</span>
    </div>
</div>


<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png">
        Мы умеем делать любые фасадные работы любой сложности. Мы понимаем, что делаем и можем, предлагая
        варианты различных решений конкретного случая, прогнозировать, как такое решение будет работать. Мы уже делали
        много декорэлементов, барельефов и все эти сложные работы до сих пор служат заказчикам без каких-либо проблем.
    </p>
</div>

<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png">
        Руководитель компании Г. Емeльянов участвует в различных научных конференциях, занимается обучением фасадным
        технологиям, участвовал в разработке нового ГОСТа 15588-2014 &quot;Плиты пенополистирольные. Технические условия&quot;
        и имеет отношение к совершенствованию строительных норм, правил и Законов РФ.
    </p>
</div>



<div class="col-sm-12">
    <div class="col-md-6 col-md-offset-3 second_home">
        <iframe src="https://www.youtube.com/embed/VbwEaj8pBNo?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
</div>



<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png">

        По своей деятельности в фасадной теплоизоляции многие известные компании, производители теплоизоляционных и
        системных материалов обращаются с различными просьбами о квалифицированной помощи.<br>
        Так,  Г. Емeльянов помогал в обучении, разработке документации и сертифицировании:<br />
        - для компании &quot;<a href="http://www.rockwool.ru" target="_blank">РОКВУЛ</a>&quot; в 2005 году были разработаны
        инструкции для технологических карт сухих смесей системы &quot;<a href="http://www.rockwool.ru/systems/rockfacade/system" target="_blank">РОКФАСАД</a>&quot;<br />
        - для компании <a href="http://www.kreisel.ru" target="_blank">KREISEL</a> был разработан в 2005 году &quot;Альбом
        технических решений&quot;, приведена в порядок вся проектная и техническая документация,
        <a href="http://www.wdvs.ru/fotogalereya/category/7-zlatoust-reportaz-sertifikatsia.html" target="_blank">оказана
            помощь</a> в первой сертификации фасадной системы KREISEL-Turbo на пенополистироле в Госстрое РФ с получением
        самого высокого класса пожарной безопасности К0. От компании KREISEL проводилось обучение сотрудников и дилеров
        на теоретических и практических семнарах по вопросам применения продукции KREISEL в строительстве и в системах
        утепления (<a href="http://www.kreisel.ru/news/a2005/grudzien2005" target="_blank">см.1</a> ,
        <a href="http://www.kreisel.ru/news/a2005/wrzesien_pazdziernik2005" target="_blank">см.2</a> ,
        <a href="http://www.kreisel.ru/news/a2006/bc2006" target="_blank">см.3</a>), даже придумано название для
        нового тогда продукта - силикатно-силиконовой штукатурки &quot;Silint-Putz&quot;.<br />
        - помощь в обучении сотрудников и дилеров компании <a href="http://www.tn.ru/" target="_blank">ТЕХНОНИКОЛЬ</a><br />
        - для компании <a href="http://www.baumit.ru" target="_blank">BAUMIT</a> в 2008-м году разработан &quot;Альбом
        технических решений&quot;<br />
        - для компании <a href="http://www.penoplex.ru" target="_blank">ПЕНОПЛЭКС</a> в 2010-м разработана технология
        тепло- гидроизоляции с помощью экструдированного пенополистирола и ПВХ-мембраны<br />
        - для компании &quot;<a href="http://www.rdigroup.ru/" target="_blank">RDI grup</a>&quot;  выполнен доклад по проблемам
        применения газобетона в строительстве<br />
        - для компании <a href="http://www.bolars.ru" target="_blank">БОЛАРС</a> была оказана помощь в сертификации
        фасадной системы на пенополистироле &quot;БОЛАРС ТВД&quot; с получением самого высокого класса пожарной безопасности К0,
        разработан &quot;Альбом типовых узлов и технических решений&quot; по фасадной системе.<br />
        - для компании <a href="http://www.penoplex.ru" target="_blank">ПЕНОПЛЭКС</a> в 2013 г. выполнены расчёты в
        применении XPS в стеновых конструкциях и подготовлена статья о применимости непаропроницаемых материалов в стенах<br />
        <br />
    </p>
</div>

<div class="col-sm-12">
    <p class="fs-16">
        Для компаний Роквул, Крайзель, Канстрой разработаны и изготовлены оригинальные рекламные макеты, которые с лучшей стороны представляют строительные системы.
    </p>
</div>

<div class="bg-grey-lite fasad3-img_block">
    <div class="col-sm-12">
        <div class="per-20"> <a href="photogallery/other/pism_baumit.jpg" rel="lightbox"><img src="photogallery/other/pism_baumit_m.jpg"></a></div>
        <div class="per-20"> <a href="photogallery/other/pism_tehn1.jpg" rel="lightbox"><img src="photogallery/other/pism_tehn1m.jpg"></a></div>
        <div class="per-20"> <a href="photogallery/other/pism_tehn2.jpg" rel="lightbox"><img src="photogallery/other/pism_tehn2m.jpg"></a></div>
        <div class="per-20"> <a href="photogallery/other/knauf-diplom.jpg" rel="lightbox"><img src="photogallery/other/knauf-diplom_m.jpg"></a></div>
        <div class="per-20"> <a href="photogallery/other/kreisel-diplom.jpg" rel="lightbox"><img src="photogallery/other/kreisel-diplom_m.jpg"></a></div>
    </div>
    <div class="col-sm-12">
        <div class="per-20"> <a href="photogallery/other/rockwool-diplom.jpg" rel="lightbox"><img src="photogallery/other/rockwool-diplom_m.jpg"></a></div>
        <div class="per-20"> <a href="photogallery/other/kreisel_alb.jpg" rel="lightbox"><img src="photogallery/other/kreisel_alb_m.jpg"></a></div>
        <div class="per-20"> <a href="photogallery/other/baumit_alb.jpg" rel="lightbox"><img src="photogallery/other/baumit_alb_m.jpg"></a></div>
        <div class="per-20"> <a href="photogallery/other/bolars1.jpg" rel="lightbox"><img src="photogallery/other/bolars1m.jpg"></a></div>
        <div class="per-20"> <a href="photogallery/other/bolars2.jpg" rel="lightbox"><img src="photogallery/other/bolars2m.jpg"></a></div>
    </div>
</div>


<div class="col-sm-12 mt-50">
    <p class="fs-16"><img src="img/good.png">
        Мы постоянно следим за новинками на строительном рынке, внедряем полезное в производство, предлагаем современные
        и интересные архитектурные решения. Для заказчиков предлагаем различные современные проверенные технологические
        решения. Все предлагаемые новинки исследуем, испытываем, после чего только принимаем решение применять или нет.
        После изучения многих рекламируемых технологий, которые попадаются на глаза в связи с агрессивной рекламой мы
        полностью исключили даже какие-либо переговоры в применении ряда материалов и технологий, которые на слуху в
        связи с их неприменимостью в нашей работе и сильным преувеличением их свойств и цены. Таких бесполезных с нашей
        точки зрения материалов очень много на рынке и мы избавляем заказчика от бесполезных, а то и вредных затрат.
    </p>
</div>

<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png">
        Для заказчиков мы можем выполнить весь комплекс работ по обоснованию тех или иных решений, теплотехнический
        расчёт соответствия уровня теплоизоляции здания нормативной, просчитать утечки тепла программным моделированием,
        сделать проект фасада. Зачастую проектные организации не имеют в штате узких специалистов по теплотехнике и
        фасадному проектированию. В проектах или нет расчётов вообще или они выполнены поверхностно "на усмотрение
        подрядчика", а зачастую изобилуют просто ошибками и предложениями закупки и использования материалов, которые в
        данном конкретном случае абсолютно неприемлимо.

    </p>
</div>


<div class="col-md-8 col-md-offset-2">
    <div class="col-md-6" align="center">
        <a href="photogallery/other/tplt_rasch.jpg" rel="lightbox"><img class="mob-respons" src="photogallery/other/tplt_raschm.jpg"></a>
        <div class="col-sm-12" align="left">
        <span class="text-v10">Теплотехнический расчёт - выпадение и накопление конденсата, соотвествие температуры
            внутренней поверхности и теплосопротивления стены нормативным.</span>
        </div>
    </div>
    <div class="col-md-6" align="center">
        <a href="photogallery/other/tplt_rasch1.jpg" rel="lightbox"><img class="mob-respons" src="photogallery/other/tplt_rasch1m.jpg"></a>
        <div class="col-sm-12" align="left">
            <span class="text-v10">Моделирование в конструкциях температурных полей, мостиков холода, утечек тепла,
            распределения температур и расчёт теплопотерь как через всю конструкцию в целом, так и через определённую часть.</span>
        </div>

    </div>
</div>

<div class="col-sm-12 mt-50">
    <p class="fs-16"><img src="img/good.png">Руководитель компании Г. Емeльянов ведёт свой информационный портал
        <a href="http://www.wdvs.ru">www.wdvs.ru</a> где опубликовано много интересного материала по строительству,
        фасадной системе и различным вопросам строительных технологий. </p>
</div>

<div class="col-sm-12">
    <p class="fs-16"><a href="photogallery/other/luch_fasady.jpg" rel="lightbox"><img src="photogallery/other/luch_fasady_m.jpg" hspace="10" border="0" align="left"></a>
        В различных глянцевых строительных журналах участвует в "Круглых столах", пишет статьи на различную строительную тематику, к примеру:<br>
        - "<a href="http://www.wdvs.ru/novosti-portala-wdvs/obzor-rinka-mokrih-fasadov-chast1.html" target="_blank">
            Рынок мокрых фасадов в России</a>" журнал "Лучшие фасады" 2007 г<br>
        - "<a href="http://www.wdvs.ru/novosti-portala-wdvs/kruglyi-stol-veduschix-proizvoditelei-fasadnyx-sistem.html" target="_blank">
            Собрание ведущих производителей «мокрых» систем</a>" журнал "Лучшие фасады" 2005/2006<br>
        - "<a href="http://www.wdvs.ru/statyi-gennadia-emelyanova/problema-paropronitsaemosti-v-sistemah.html" target="_blank">
            Проблема паропроницаемости и подбора материалов в системах </a>" "Лучшие фасады" 2005<br>
        - <a href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/ostorozno-penopolistirol-v-stroitelstve.html">"
            Осторожно!? Пенополистирол в строительстве</a>" журналы "Еврострой" и "Коммунальный комплекс России"<br>
        - &quot;Применение паронепроницаемой теплоизоляции Пеноплэкс в строительстве&quot; (статья на
        <a href="http://www.penoplex.ru/o_kompanii/presscentr/publikacii/primenenie_paronepronicaemoj_teploizolyacii_penopleks_v_stroitelstve/" target="_blank">
            сайте Пеноплэкс</a> и на <a href="http://www.wdvs.ru/biblioteka-wdvs/penoplex-steni.html" target="_blank">
            wdvs.ru с теплотехническими расчётами</a>)</p>
</div>

<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png">Долговечность выполненной работы и  отсутствие претензий по гарантии.</p>
    <p class="fs-16"> Фасадные работы выполнялись с 1997 года, за этот период можно посмотреть выполненные нами объекты, которые
        эксплуатируются без каких-либо дополнительных затрат и ремонтов.<br>
        (<em>смотрите раздел &quot;Сделанные объекты&quot; -  объект &quot;Поселковая администрация&quot; выполнен в
            1999 г и в 2015 году отснято состояние фасада в фильме &quot;Технология СФТК&quot;</em>)</p>
</div>

<div class="col-sm-12">
    <p class="fs-16"><img src="img/good.png" alt="">Разработка и применение целого комплекса материалов и инженерных решений для повышения
    долговечности в районах декоров фасада, оконных отливов, цоколей, отмосток, примыкания к другим строительным
    конструкциям и системам (другие типы фасадов, кровля и т.п.) и в некоторых нестандартных вариантах применения фасадной отделки.
    <br>
    Данные решения мы не желаем озвучивать публично во избежание &quot;обучения&quot; наших конкурентов. Применение
    материалов и решений, определённых нюансов в работе  (почему необходимо сделать именно так) мы объясняем только в
    момент, когда мы приступаем к работе  на объекте.<br>
    По данному вопросу мы  не консультируем. Данные мероприятия носят эксклюзивный характер, так как используются
    различные уникальные решения и материалы, которые есть в ассортименте разных компаний, но широкому кругу строителей
    они не известны и не применяются массово в целях экономии.</p>
</div>

<div class="col-sm-12 fasad-next" align="center"><a href="fasad4.php">&gt; на следующую страницу &quot;Стоимость фасадных работ и материалов&quot;</a></div></td>


<?php require 'footer.php'; ?>


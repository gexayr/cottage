<?php require 'header.php'; ?>
<div class="col-sm-12">
    <div class="col-md-3">
        <div class="ofirme_block">
            <a href="photogallery/eesti/maksuamet1.jpg" rel="lightbox" title="утепление-коттеджа.рф">
                <img class="fotofon" src="photogallery/eesti/maksuamet1m.jpg">
            </a>
        </div>

        <div class="ofirme_block">
            <a href="photogallery/eesti/maksuamet5.jpg" rel="lightbox">
                <img class="fotofon" src="photogallery/eesti/maksuamet5m.jpg">
            </a>
        </div>
        <div class="ofirme_block">
            <a href="photogallery/eesti/maksuamet6.jpg" rel="lightbox">
                <img class="fotofon" src="photogallery/eesti/maksuamet6m.jpg">
            </a>
        </div>

        <div class="ofirme_block">
            <span align="center" class="text-V12blue">1997 г. - Таллинский Налоговый Департамент.<br>
                Таллин, ул. Эндла 8
            </span>
        </div>
    </div>

    <div class="col-md-9">
        <span class="ofirme_span">
            <br>
            <br>
            <br>
            <p>Работа в строительстве началась в 1994 году в Эстонии (г.
                Таллин) с производства отделочных работ. Профессиональный уровень рос с каждым годом, осваивались новые
                технологии.
            </p>
            <br>
            <p>
                В 1995 году уже выполнялись также и общестроительные работы, а также расширена география, выполняли
                элитные &quot;евроремонты&quot; в Москве.
            </p>
                <br>
            <p>
                В 1997 году была освоена технология &quot;мокрых фасадов&quot;. Для этого пригласили работать польских
                строителей, которые работали по данной фасадной технологии в Германии.<br>
                На строительном рынке была очень жёсткая конкуренция, тем не менее в этих условиях удавалось комфортно
                работать, не снижая объёмов и качества.<br>
                По состоянию на конец 90-х годов было сделано примерно 30-35 тысяч кв.м. работ по утеплению фасадов.</p>
                <br>
                <p>
                Строительно-отделочные работы выполнялись на очень известных объектах в Эстонии - Академия Наук,
                Налоговый департамент, гостиница Рэдиссон-САС, Академия &quot;Биомедикум&quot;, много офисных зданий,
                школ, детских садов и офисных зданий.</p>
        </span>
    </div>
</div>
<div class="col-sm-12">
    <hr class="for_mob">

    <div class="col-md-9 vcenter">
        <span class="ofirme_span">
            Параллельно велась работа и в России. <br>
            <br>
            Строительство и отделка объектов выполнялись в городах Санкт-Петербург, Москва, Вологда, Ярославль и областях.<br>
            <br>
            С середины 2000-х в России стала набирать популярность технология СФТК (&quot;мокрый фасад&quot;) и мы были уже подготовлены, так как имелся уже большой опыт.<br>
            <br>
            В период с 1997 г сделано порядка 120 тыс. кв.м. утеплённых фасадов, построены частные коттеджи, торговые и производственные здания.<br>
            <br>
            Очень много работы выполнено по различным строительных технологиям.<br>
            <br>
            <br>
            <br>
        </span>
    </div>
    <div class="col-md-3 ofirme_block">
        <div>
            <a href="photogallery/eesti/radisson.jpg" rel="lightbox">
                <img class="fotofon" src="photogallery/eesti/radisson_m.jpg">
            </a>
        </div>

        <span class="text-V12blue">1999 г. - г. Таллин<br>
      отель Рэдиссон-САС</span>
        <hr>
        <div>
            <a href="photogallery/stroika/nikolsk.jpg" rel="lightbox">
                <img class="fotofon" src="photogallery/stroika/nikolsk_m.jpg">
            </a>
        </div>
        <span class="text-V12blue">2003 г. - Вологодская обл.<br>
            г. Никольск, Торговый дом</span>
        </span>
        <hr>
        <div>
            <a href="photogallery/news/spb_pered.jpg" rel="lightbox">
                <img class="fotofon" src="photogallery/news/spb_pered_m.jpg">
            </a>
        </div>
        <span class="text-V12blue">2009 г. - г. Санкт-Петербург<br>
              ул. Передовиков 9 к.2, <br>
              жилой дом</span>
    </div>
</div>




<div class="col-sm-12">
    <div class="col-md-6">
        <div class="col-sm-12">
            <div class="col-sm-6 partner-item partner-item" style="margin-top: 8%">
                <a href="http://www.sibur.ru" target="_blank">
                    <img src="img/Sibur2016.gif">
                </a>
            </div>
            <div class="col-sm-6 partner-item">
                <a href="http://www.alphapor.ru/" target="_blank">
                    <img src="img/alphapor.gif">
                </a>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6 partner-item">
                <a href="http://www.epsrussia.ru" target="_blank">
                        <img src="img/eps_logo.gif">
                </a>
            </div>
            <div class="col-sm-6 partner-item">
                <a href="http://anfas.biz" target="_blank">
                    <img src="img/anfas.gif">
                </a>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6 partner-item">
                <a href="http://www.penoplex.ru" target="_blank">
                    <img src="img/banner_penoplex_p_134x129.gif">
                </a>
            </div>
            <div class="col-sm-6 partner-item">
                <a href="http://baumit.ru">
                    <img src="img/baumit_small.gif">
                </a>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6 partner-item">
                <a href="http://www.kreisel.ru" target="_blank">
                    <img src="img/kreisel_logo.gif">
                </a>
            </div>
            <div class="col-sm-6 partner-item">
                <a href="http://www.ceresit.ru" target="_blank">
                    <img src="img/logo_ceresit.gif">
                </a>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6 partner-item">
                <a href="http://www.ms31.ru" target="_blank">
                    <img src="img/logoMS-31.gif">
                </a>
            </div>
            <div class="col-sm-6 partner-item">
                <a href="http://www.knauf.ru" target="_blank">
                    <img src="img/knauf_logo.gif">
                </a>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6 partner-item">
                <a href="http://www.bolars.ru" target="_blank">
                    <img src="img/logo_Bolars.gif">
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-6 vcenter">
           <span class="ofirme_span">Кроме непосредственно подрядных работ по строительству и проектирования,
                в России сотрудничаем с основными игроками на строительном рынке производства сухих смесей и теплоизоляции
                по подготовке технической документации, сертификации, обучению, внедрению новых технологий.<br>
                <br>
                Совместные проекты были и продолжают осуществляться с такими компаниями, как:<br>
                <br>
                Сибур, Ассоциация Производителей и Поставщиков Пенополистирола, Ассоциация производителей и поставщиков фасадных систем
                &quot;Анфас&quot;, Пеноплэкс, Мосстрой-31, Кнауф, Технониколь, Крайзель, Баумит, Церезит, Боларс.
                <br>
          </span>
    </div>
</div>





      <div class="col-sm-12">
          <div class="ofirme-video" align="center">Высокий профессионализм доказывают такие факты, как, например, приглашение на канал РБК в качестве эксперта:<br>
            <br>
              <iframe src="https://www.youtube.com/embed/vjwqm5SUX3g" frameborder="0" allowfullscreen></iframe>
          </div>
          <br>
            ... помощь в сертифкации систем фасадного утепления (компании Крайзель, Баумит, Боларс), участие в различных конференциях, разработка технической документации.<br>
            <br>
          На данный момент производим обучающие видеофильмы по строительным технологиям, которые доступны для просмотра на канале Ютуб:
            <a href="http://www.youtube.com/user/8115522" target="_blank">http://www.youtube.com/user/8115522</a>

          <hr>
      </div>

<div class="col-sm-8 col-sm-offset-2">
      <span><strong>На сегодня предлагаем следующие услуги:</strong><br>
          <br>
    - проектирование и строительство объектов различного уровня сложности и назначения<br>
    <br>
    - технический надзор за строительством, отделкой и фасадными работами<br>
    <br>
    - специализируемся на системах теплоизоляции СФТК (&quot;мокрый фасад&quot;), но выполняем также любые другие фасадные работы<br>
    <br>
    - выполнение монтажа инженерных систем и отделочных работ<br>
    <br>
    - помощь в сертификации фасадных систем в Минстрое РФ для производителей фасадных систем и материалов<br>
    <br>
    - разработка технической документации по строительным материалам и технологиям<br>
    <br>
    - обучение строительным технологиям<br>
    <br>
    - производство видеофильмов (специализация по строительным технологиям)
      <br>
      <br>
      </span>
</div>
<?php require 'footer.php'; ?>

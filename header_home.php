<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Утепление-коттеджа.рф</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">

    <!--    bootstrap-->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="/litebox.js"></script
    <link rel="stylesheet" type="text/css" href="style/litebox.css">
    <link rel="stylesheet" type="text/css" href="style/styles.css">
    <script>
        $(document).ready(function(){
            $("#navbar_button").click(function(){
                $("#second_level_menu").fadeToggle();
            });
        });
    </script>
</head>
<body>

<div class="container">
        <div class="row">
                <a href="/">
                    <img src="img/logo.jpg" alt="Утепление коттеджа - утепление-коттеджа.рф"
                         class="img-responsive respons">
                </a>
            <div class="col-sm-12">
                <div class="col-md-4" align="center" valign="middle">
                    <img src="img/pic01.jpg" width="187" height="140">
                    <p class="spis2">- проектирование<br>
                        - общестроительные работы и отделка<br>
                        - монтаж инженерных систем</p>
                </div>
                <div class="col-md-4" align="center" valign="middle">
                    <img src="img/pic02.jpg" width="183" height="140">
                    <p class="spis2">- утепление фасадов зданий любой сложности и этажности<br>
                        - изготовление декоративных элементов</p>
                </div>
                <div class="col-md-4" align="center" valign="middle">
                    <img src="img/pic03.jpg" width="172" height="140">
                    <p class="spis2">- разработка технической документации<br>
                        - видеосъёмка строительных технологий<br>
                        - помощь в сертификации</p>
                </div>
            </div>
            <div class="">
                <div class="col-sm-12" align="center"><span class="recommend rekom">РЕКОМЕНДУЕМ ПОСЕТИТЬ :</span></div>
                <div class="col-sm-12 bg-grey-lite">
                    <div class="col-sm-6">
                        <a href="http://www.wdvs.ru" target="_blank" class="res">
                        <span class="col-md-3">
                            <img src="img/res1b.gif" class="header-image" align="left">
                        </span>
                            <span class="col-md-9">
                            <p>Персональный информационный портал Г.Емeльянoва. Максимум информации о &quot;мокром фасаде&quot;
                            (СФТК), статьи и расчёты по различным строительным технологиям.</p>
                        </span>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="http://hochusebedom.ru" target="_blank" class="res">
                        <span class="col-md-3">
                            <img src="img/res2b.gif" class="header-image" align="left">
                        </span>
                            <span class="col-md-9">
                            <p>Строительный форум для общения. Если вам необходимо что-то узнать,
                            задайте вопрос, вам ответят специалисты и разбирающиеся в интересующейся теме посетители.</p>
                        </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
<?php require 'menu.php'?>

<?php require 'header.php'; ?>

<div class="col-md-12 mt-50">
    <div class="col-md-5">
        <img src="img/stroika1.jpg" class="respons">
    </div>
    <div class="col-md-7 mt-50">
        <p><strong>Выполняем все строительные и отделочные работы.<br>
                Как отдельные этапы, так и полностью - от проектирования до сдачи объекта &quot;под ключ&quot;.</strong><br>
            <br>
            Большой опыт работы в строительстве и отделке с 1994 года.</p>
        <p>
            <span class="text-V12">За всё время своей работы мы сделали так много различной работы, что что-то выделить
                отдельно просто невозможно. Это и фундаменты, несущие конструкции, кровли, отделка внутренних помещений,
                монтаж систем электроснабжения, отопления и водоснабжения, планировка территории.</span><br>
        </p>
    </div>
</div>


<div class="row stroy-first">
    <div class="col-sm-12 border-b">
        <span align="center" class="text-V12 stroy-span">Мы делимся информацией, обучаем технологиям, сотрудничая с известными компаниями:</span>
    </div>
    <div class="col-sm-12 bg-grey">
        <div align="center">
            <span class="fs-18">Видеоинструкции по строительно-отделочным технологиям</span><br>
            <span class="text-V12">все работы в представленных фильмах выполнены нами</span>
        </div>
    </div>
</div>


<div class="col-sm-12">
    <hr>
    <div class="col-md-5 second_home">
        <iframe src="https://www.youtube.com/embed/Jtw7-0XAyQo?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-7 mt-50">
        <p> <strong>Технология выполнения полусухой стяжки самым надёжным и недорогим способом.</strong><br>
            <br>
            Выполнение работ подробно описано на сайте <span class="red">www.wdvs.ru</span>
            <a href="http://www.wdvs.ru/tehnologii-ot-gennadiya-emelyanova/gennady-polusuhya-styazka.html" target="_blank">(ссылка)</a>.</p>
        <p>Для компаний &quot;Сибур&quot; и &quot;Ассоциации производителей и поставщиков пенополистирола&quot; мы сняли обучающий видеоролик,
            как правильно делается полусухая стяжка по утеплителю - пошаговая инструкция.
        </p>
    </div>
</div>



<div class="col-sm-12">
    <hr>
    <div class="col-md-5 second_home">
        <iframe src="https://www.youtube.com/embed/vumimOLpKXY?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-7 mt-50">
        <p><strong>Технология выполнения стяжки при помощи смеси CERESIT CN-185 с укладкой электрического шнура обогрева.</strong></p>
        <p>Для компании CERESIT мы сняли обучающий ролик по изготовлению стяжки из материалов компании.</p>
    </div>
</div>


<div class="col-sm-12">
    <hr>
    <div class="col-md-5 second_home">
        <iframe src="https://www.youtube.com/embed/XHEo5R1BKHg?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-7 mt-50">
        <p><strong>Утепление лоджии</strong></p>
        <p>Для компаний &quot;Сибур&quot; и &quot;Ассоциации производителей и поставщиков пенополистирола&quot; мы сняли обучающий
            видеоролик, как правильно делается утепление лоджии - пошаговая инструкция.</p>
    </div>
</div>


<div class="col-sm-12">
    <hr>
    <div class="col-md-5 second_home">
        <iframe src="https://www.youtube.com/embed/SocctaXFRwY?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-7 mt-50">
        <p><strong>Строительство энергоэффективных зданий по каркасной технологии</strong></p>
        <p>Для компаний &quot;Сибур&quot; и &quot;Ассоциации производителей и поставщиков пенополистирола&quot; мы сняли обучающий
            видеоролик, как правильно проектировать, строить и утеплять каркасные здания,  чтобы значительно
            сэкономить на отоплении и кондиционировании - пошаговая инструкция.</p>
    </div>
</div>


<div class="row stroy-first">
    <div class="col-sm-12 border-b">
        <span align="center" class="text-V12 stroy-span">Фотографии и видео с некоторых наших объектов:</span>
    </div>
    <div class="col-sm-12 bg-grey">
        <div align="center">
            <span class="fs-18">Строительство коттеджа (Смоленская область)</span>
        </div>
    </div>
</div>


<div class="col-md-12 second-proekt">
    <div class="col-md-5">
        <a href="photogallery/stroika/ct1.jpg" rel="lightbox">
            <img class="fotofon fotofon-big mob-respons" src="photogallery/stroika/ct1m.jpg">
        </a>
    </div>
    <div class="col-md-7">
        <p class="fs-18">К котловану и его подготовке под строительство здания - максимальное внимание. Всё делается
            очень аккуратно, последовательно.
            Все уровни, все слои проверяются по лазерному нивелиру, имеющему собственную погрешность 0,1мм/1м.<br>
            Все необходимые подготовительные слои (щебень, песок) тщательно утрамбовываются виброплитой, чтобы будущее
            здание не имело ни сантиметра просадки.</p>
    </div>
</div>


<div class="col-md-12 second-proekt">
    <div class="col-md-5">
        <a href="photogallery/stroika/ct6.jpg" rel="lightbox">
            <img class="fotofon fotofon-big mob-respons" src="photogallery/stroika/cot06mm.jpg">
        </a>
    </div>
    <div class="col-md-7 mt-50">
        <p class="fs-18">При выборке грунта экскаватором по лазерному нивелиру отслеживается, чтобы лишнего не убрал из
            котлована, дальше котлован дорабатывается вручную лопатами до проектных отметок.<br>
            На фото пример - лежит доска 6 метров длиной, показана ровность, с какой сделано дно котлована. По опыту -
            у многих даже в квартирах не получаются чистовые стяжки такой ровности.</p>
    </div>
</div>

<div class="col-md-12 second-proekt">
    <div class="col-md-5">
        <a href="photogallery/stroika/ct2.jpg" rel="lightbox">
            <img class="fotofon fotofon-big mob-respons" src="photogallery/stroika/ct2m.jpg">
        </a>
    </div>
    <div class="col-md-7">
        <p class="fs-18">Для выполнения бетонных работ на данном объекте были задействованы проверенные рабочие-
            бетонщики из Эстонии. Многие знают, что рабочие из Прибалтики выполняют строительные работы очень качественно.
            Да, стоимость их работ выше среднего по РФ, но результат того стоит.<br>
            Бетонщики работают в-основном в Финляндии и Швеции, а там криворуких и безответственных не берут на выполнение
            работ.<br>
            На фото: рабочий по лазерному уровню контролирует укладку бетона на силовую стяжку фундамента.</p>
    </div>
</div>

<div class="col-md-12 second-proekt">
    <div class="col-md-5">
        <a href="photogallery/stroika/ct24.jpg" rel="lightbox">
            <img class="fotofon fotofon-big mob-respons" src="photogallery/stroika/cot24mm.jpg">
        </a>
    </div>
    <div class="col-md-7 mt-50">
        <p class="fs-18">В строительстве очень важно все несущие конструкции сделать максимально качество. Это влияет
            на безопасность, на здоровье и жизнь. Здесь не должно быть поверхностного отношения к работе.<br>
            На фото подготовленное перекрытие к заливке бетоном. Арматура разложена в соответствии с проектом и
            максимально отвественно.</p>
    </div>
</div>


<div class="col-md-12 second-proekt">
    <div class="col-md-5">
        <a href="photogallery/stroika/ct44.jpg" rel="lightbox">
            <img class="fotofon fotofon-big mob-respons" src="photogallery/stroika/cot44mm.jpg">
        </a>
    </div>
    <div class="col-md-7 mt-50">
        <p class="fs-18">Не бывает не нужных мелочей. Всё должно быть продумано и удобно для владельца.<br>
            На фото - строительство и наружная отделка подходят к концу, делаются фасадные работы. Так как
            дом стоит на высокой точке в посёлке, то сделаны и заземлены молниеотводы.</p>
    </div>
</div>


<div class="col-sm-12">
    <hr>
    <div class="col-md-5 second_home">
        <iframe src="https://www.youtube.com/embed/hMP-9l2XWeU?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-7 mt-50">
        <p class="fs-18">Видеоролик о начале строительства - подготовка котлована.</p>
    </div>
</div>



<div class="col-sm-12">
    <hr>
    <div class="col-md-5 second_home">
        <iframe src="https://www.youtube.com/embed/D2ST8TEqCCQ?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-md-7 mt-50">
        <p class="fs-18">Видеоролик - изготовление декорэлементов и окончание отделки фасада</p>
    </div>
</div>



<div class="col-sm-12 mt-50">
            <p class="fs-18" align="center">Фотографии строительства данного объекта:</p>
</div>


         <div class="col-sm-12 mt-50" align="center">
             <div class="col-md-3"><a href="photogallery/stroika/cot01.jpg" rel="lightbox">
                     <img class="fotofon" src="photogallery/stroika/cot01m.jpg">
                 </a>
             </div>
             <div class="col-md-3"><a href="photogallery/stroika/cot02.jpg" rel="lightbox">
                     <img class="fotofon" src="photogallery/stroika/cot02m.jpg">
                 </a>
             </div>
             <div class="col-md-3"><a href="photogallery/stroika/cot03.jpg" rel="lightbox">
                     <img class="fotofon" src="photogallery/stroika/cot03m.jpg">
                 </a>
             </div>
             <div class="col-md-3"><a href="photogallery/stroika/cot04.jpg" rel="lightbox">
                     <img class="fotofon" src="photogallery/stroika/cot04m.jpg">
                 </a>
             </div>
         </div>

         <div class="col-sm-12" align="center">
             <div class="col-md-3"><a href="photogallery/stroika/cot05.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot05m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot06.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot06m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot07.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot07m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot08.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot08m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot09.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot09m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot10.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot10m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot11.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot11m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot12.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot12m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot13.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot13m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot14.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot14m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot15.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot15m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot16.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot16m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot17.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot17m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot18.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot18m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot19.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot19m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot20.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot20m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot21.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot21m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot22.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot22m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot23.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot23m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot24.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot24m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot25.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot25m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot26.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot26m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot27.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot27m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot28.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot28m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot29.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot29m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot30.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot30m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot31.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot31m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot32.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot32m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot33.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot33m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot34.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot34m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot35.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot35m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot36.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot36m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot37.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot37m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot38.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot38m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot39.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot39m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot40.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot40m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot41.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot41m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot42.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot42m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot43.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot43m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot44.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot44m.jpg"></a></div>
         </div>
         <div align="center" class="col-sm-12">
             <div class="col-md-3"><a href="photogallery/stroika/cot45.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot45m.jpg"></a></div>
             <div class="col-md-3"><a href="photogallery/stroika/cot46.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/cot46m.jpg"></a></div>
         </div>


<div class="stroy-first">
    <div class="col-sm-12 bg-grey mt-50">
        <div align="center">
            <span class="fs-18">Строительство завода по производству товарного бетона (г.Ивантеевка)</span>
        </div>
    </div>
</div>

<div align="center" class="col-sm-12 mt-50">
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka01.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka01m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka02.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka02m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka03.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka03m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka04.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka04m.jpg"></a></div>
</div>
<div align="center" class="col-sm-12">
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka05.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka05m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka06.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka06m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka07.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka07m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka08.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka08m.jpg"></a></div>
</div>
<div align="center" class="col-sm-12">
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka09.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka09m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka10.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka10m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka11.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka11m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka12.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka12m.jpg"></a></div>
</div>
<div align="center" class="col-sm-12">
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka13.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka13m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka14.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka14m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka15.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka15m.jpg"></a></div>
    <div class="col-md-3"><a href="photogallery/stroika/ivanteevka16.jpg" rel="lightbox"><img class="fotofon" src="photogallery/stroika/ivanteevka16m.jpg"></a></div>
</div>


<span class="tags">строительство коттеджа, где заказать проект, стоимость строительства, цена строительных работ,
            сколько стоит построить коттедж, где найти рабочи газобетон газосиликат купить блоки купить пенобетон пеноблоки
            где найти рабочих на фасад готовые проекты купить цена строительные работы расценки за работу отделка отделочные
            работы как построить коттедж выполняем фасадные работы делаем утепление коттеджа утепляем фасад теплоизоляция
            пенопласт пенополистирол стекловата каменная вата минвата где купить клей выбрать обои медные трубы
            цена металлопластиковые трубы полипропиленовые пластиковые сделать трубы спаять рабочие на фасад какой
            толщины утепление какой толщины кирпичная стена из газоблоков из гасиликатных пеноблоков какой толщины
            утеплитель найти строителей фасадчиков фасадные работы термокраска стоимость фасадных работ компания
            выполняющая строительные отделочные фасадные работы украсить фасад изготовление декорэлементов декоративных
            элементов купить готовые декоры на фасад декоры из пенополистирола полиуретана напыление пенополиуретаном
            ППУ кладка блоков цена клея для кладки газосиликатных пеноблоков где купить плиточный клей обои фанеру
            кирпич цена под ключ строительства квартирный ремонт как утеплить лоджию как утеплить стены почему мокнут
    стены углы как где найти рабочих на фасад сделать отделку фасада утеплитьх, цена газосиликата, цена газобетона,
    экструдированный пенополистирол, гидроизоляция, цена изоляции, сколько стоит пенобетон  проект каркасного дома
    бесплатный проект готовый проект купить каркасник брусовый дом чем утеплить стену как построить каркасный дом
    чем отделать фасад брусового коттеджа горючесть горит утеплитель пенопласт пенополистирол полиуретан пеноизол
    СИП панели кирпичная кладка поротерм поризованная керамика кирпичная кладка цена стоимость где купить цены
    построить коттедж купить земельный участок</span>

<?php require 'footer.php'; ?>

-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Июн 26 2019 г., 13:46
-- Версия сервера: 5.7.21-20-beget-5.7.21-20-1-log
-- Версия PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `gexo777_kotej`
--

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--
-- Создание: Июн 14 2019 г., 21:02
-- Последнее обновление: Июн 20 2019 г., 11:50
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `menu` tinyint(1) DEFAULT '1',
  `parent` int(11) DEFAULT '0',
  `content` text,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `title`, `alias`, `menu`, `parent`, `content`, `date`) VALUES
(1, 'тест', '/test', 0, 0, '<p>тест</p>', '2019-06-10 13:41:12'),
(8, 'test submenu', '/proekti23', 0, 0, '<p></p>', '2019-06-13 17:25:50'),
(10, 'УШП - Утеплённая Шведская Плита', '/stroitelstvo2', 1, 2, '<p>УШП тут раздел будет про фундамент УШП<br><br><br></p>', '2019-06-15 12:02:36'),
(11, 'КОНТАКТЫ', '/contacts.php', 1, 0, '<p>Уважаемые!\r\n</p><p>Прошу понять.<br><strong>Я не оказываю консультации по телефону и емайлу. </strong><br>Пожалуйста, очень прошу, не отвлекайте тем, что вы бы хотели узнать, какой материал выбрать в том или ином вашем случае или у кого что лучше купить.<br><br>Я бываю часто занят, просто нет времени и решаю все рабочие моменты и вопросы только со своими заказчиками.\r\n</p><p>Если вы хотите пообщаться, в каком случае и как лучше что-либо сделать, <a href=\"http://hochusebedom.ru/\" target=\"_blank\">то зайдите на форум</a> и там можно к вопросу приложить фото или видео. Как у меня будет время - я отвечу.\r\n</p><p>Спасибо за понимание!\r\n</p><p><br><br>\r\n</p><p>Для общения именно по работе и рабочим моментам контакты:\r\n</p><p>моб.тел. <a class=\"contacts\" href=\"tel:+7916 9603374\">+7 916 960 33 74</a>   + (WhatsApp)\r\n</p><p>емайл: \r\n	<a class=\"contacts\" href=\"mailto:wdvs.ru@gmail.com\">wdvs.ru@gmail.com</a>\r\n</p><p><br><br>\r\n</p><p>C уважением, <br>Геннaдий Емeльянов\r\n</p><p><a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\">Обратная связь</a>\r\n</p>', '2019-06-16 19:41:24');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--
-- Создание: Июн 14 2019 г., 21:02
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `login`, `password`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

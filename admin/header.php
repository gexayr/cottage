<?php
if (!$_COOKIE["key"])header("Location: /admin/login.php");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta required name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/admin/plugins/redactor/redactor.css">
    <link rel="stylesheet" href="styles.css">
</head>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">утепление-коттеджа.рф</a>
        </div>
        <?php if(isset($_COOKIE["key"])){ ?>
            <ul class="nav navbar-nav">
                <li><a href="/admin/">Страницы</a></li>
                <li><a href="/admin/add.php">Добавить Страницу</a></li>
            </ul>
        <?php } ?>

        <ul class="nav navbar-nav navbar-right">
            <?php if(isset($_COOKIE["key"])){ ?>
                <li><a href="logout.php" tite="Logout"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
            <?php } else { ?>
                <li><a href="login.php" tite="Logout"><span class="glyphicon glyphicon-log-in"></span> Вход</a></li>
            <?php } ?>
        </ul>
    </div>
</nav>
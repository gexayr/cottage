<?php
require "header.php";

require "../pdo/config.php";
require "../pdo/common.php";



if(isset($_POST['submit'])) {
    if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();
    try {
        $connection = new PDO($dsn, $username, $password, $options);
        $menu = 0;
        $parent = 0;
        if (isset($_POST['menu']) && $_POST['menu'] == 'on') $menu = 1;
        if ($_POST['parent'] != '') $parent = $_POST['parent'];

        $edit_page = array(
            "id"        => $_POST['id'],
            "title" => $_POST['title'],
            "alias" => $_POST['alias'],
            "menu" => $menu,
            "parent" => $parent,
            "content" => $_POST['content'],
        );
        $sql = "UPDATE pages 
            SET title = :title, 
              alias = :alias, 
              menu = :menu, 
              parent = :parent, 
              content = :content
            WHERE id = :id";

        $statement = $connection->prepare($sql);
        $statement->execute($edit_page);


        echo '     <div class="alert alert-success alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            Вы <strong>Успешно</strong> отредактировали страницу.
        </div>';
    } catch (PDOException $error) {
//        echo $sql . "<br>" . $error->getMessage();
        echo '     <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> ' . $error->getMessage() . '
        </div>';
    }

}

if(isset($_GET['id'])) {
    try {
        $connection = new PDO($dsn, $username, $password, $options);
        $sql = "SELECT * FROM pages
    where id = :id";
        $statement = $connection->prepare($sql);
        $statement->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll();
//dd($result);
        if ($result && $statement->rowCount() > 0) {
            $page = $result[0];
        }

    } catch (PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
        die;
    }
}
try  {
    $connection = new PDO($dsn, $username, $password, $options);

    $sql = "SELECT * 
            FROM pages
            WHERE parent = :parent";

    $parent = 0;
    $statement = $connection->prepare($sql);
    $statement->bindParam(':parent', $parent, PDO::PARAM_STR);
    $statement->execute();

    $pages = $statement->fetchAll();
} catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
?>

<div class="container">
        <div class="row">

            <h2 align="center" style="margin-top: 74px;">Редактирование Страницы</h2>
            <?php if(empty($result)){ ?>
                <h2 align="center"> такой страницы не нашлось </h2>
            <?php } else { ?>
            <form class="form-horizontal" action="edit.php?id=<?=$page['id']?>" method="post">
                <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
                <input name="id" type="hidden" value="<?php echo escape($page['id']); ?>">
                <div class="form-group">
                    <label for="usr">Загаловок:</label>
                    <input type="text" class="form-control" name="title" required value="<?=$page['title']?>">
                </div>
                <div class="form-group">
                    <label for="usr">Адрес:</label>
                    <input type="text" class="form-control" name="alias" placeholder="/example" required value="<?=$page['alias']?>">
                    <p>* если хотите добавить страницу в подкатегорию существующей статической страницы (НА ГЛАВНУЮ,
                        О КОМПАНИИ, НОВОСТИ, ПРОЕКТИРОВАНИЕ, СТРОИТЕЛЬСТВО, ФАСАДНЫЕ РАБОТЫ, КОНТАКТЫ), то напишите адрес
                        похожим на родительскую. (напримаер для страницы СТРОИТЕЛЬСТВО нужно написать stroitelstvo1 или stroitelstvo_test)</p>
                </div>
                <div class="form-group">
                    <label for="usr">Родительская страница:</label>
                    <select name="parent" class="form-control">
                        <option value="">Без Родительской Страницы</option>
                        <option value="2">Статическая страница</option>
                        <?php foreach ($pages as $item) {
                            if($item['id'] != ($page['id'])) {
                                ?>
                            <option value="<?=$item['id']?>" <?php if($page['parent'] == $item['id'])echo 'selected';?>><?=$item['title']?></option>
                     <?php  }
                        }?>
                    </select>
                    <p>* для динамических страниц достаточно только выбрать родительскую страницу, адрес не обязательно
                        должно быть похожим на адрес родительской страницы </p>
                </div>
                <div class="form-group">
                    <label for="usr">Добавит в меню? </label>
                    <input type="checkbox" name="menu" <?=($page['menu'])?'checked':''?>>
                </div>
                <div class="form-group">
                    <label>Контент</label>
                    <textarea name="content" id="redactor">
                        <?=$page['content']?>
                    </textarea>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 pull-right">
                        <input type="submit" value="Сохранить" name="submit" class="btn btn-primary">
                    </div>
                </div>
            </form>
            <?php } ?>
        </div>
    </div>
<?php require "footer.php"; ?>
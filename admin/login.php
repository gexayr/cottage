<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta required name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <!-- fa-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="styles.css">
</head>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">утепление-коттеджа.рф</a>
        </div>
    </div>
</nav>

<?php
ini_set("display_errors",1);
error_reporting(E_ALL);
require "../pdo/config.php";
require "../pdo/common.php";


if (isset($_POST['submit'])) {

    print_r($_SESSION['csrf']);
    echo "<br>";
    print_r( $_POST['csrf']);
    if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die('csrf');
    try  {
        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT * 
            FROM settings
            WHERE login = :login 
            AND password = :password";

        $login = $_POST['login'];
        $password = md5($_POST['password']);
        $statement = $connection->prepare($sql);
        $statement->bindParam(':login', $login, PDO::PARAM_STR);
        $statement->bindParam(':password', $password, PDO::PARAM_STR);
        $statement->execute();

        $result = $statement->fetchAll();
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();

    }
}
?>

<?php
if (isset($_POST['submit'])) {
    if ($result && $statement->rowCount() > 0) {
        $cookie_name = "name";
        $cookie_value = $_POST['login'];
        setcookie($cookie_name, $cookie_value, time() + (3600 * 18), "/admin"); // 86400 = 1 day
        $cookie_name = "key";
        $cookie_value = md5($_POST['login']);
        setcookie($cookie_name, $cookie_value, time() + (3600 * 18), "/admin"); // 86400 = 1 day
//        sleep(1);
//
        header('Location: /admin/');

    } else { ?>
        <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center; margin-bottom: 0; margin-top: 10px">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> Login or password not found.
        </div>
    <?php }
} ?>

<div class="container">
    <h2 class="text-center" style="margin-top: 70px">Login</h2>
    <div class="card card-container col">
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin col-sm-4 col-sm-offset-4" method="post">
            <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">

            <span id="reauth-email" class="reauth-email"></span>
            <div class="form-group">
                <input type="text" id="inputEmail" class="form-control" placeholder="Login" required autofocus name="login">
            </div name="">
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password">
            <div id="remember" class="checkbox">

            </div>
            <input class="btn btn-info col-sm-4 col-sm-offset-4" type="submit" name="submit" value="Sign in">
        </form><!-- /form -->

    </div><!-- /card-container -->
</div><!-- /container -->

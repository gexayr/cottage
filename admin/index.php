<?php
ob_start();
require "../pdo/common.php";
require "../pdo/config.php";
require "header.php";

    try  {
        $connection = new PDO($dsn, $username, $password, $options);
        $sql = "SELECT * FROM pages";
        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
        die;
    }
?>
<div class="container">
    <div class="row col-md-10 col-md-offset-1 custyle">
        <table class="table table-striped custab">
            <thead>
            <?php
            if(empty($result))die('нет страниц');
            ?>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Parent</th>
                <th>Menu</th>
                <th>Alias</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <?php
                foreach ($result as $page){
            ?>
            <tr>
                <td><?=$page['id']?></td>
                <td><?=$page['title']?></td>
                <td><?=($page['parent'] == 2)?'Статическая страница':$page['parent']?></td>
                <td><?=($page['menu'])?'да':'нет'?></td>
                <td><?=($page['alias'])?></td>
                <td class="text-center">
                    <a href="edit.php?id=<?=$page['id']?>" class='btn btn-info btn-xs'>
                        <span class="glyphicon glyphicon-edit"></span> Edit
                    </a>
                    <a href="#" class="btn btn-danger btn-xs" onclick="removePage(<?=$page['id']?>, this)">
                        <span class="glyphicon glyphicon-remove"></span> Del
                    </a>
                </td>
            </tr>
            <?php } ?>
<!--            <tr>-->
<!--                <td>2</td>-->
<!--                <td>Products</td>-->
<!--                <td>Main Products</td>-->
<!--                <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>3</td>-->
<!--                <td>Blogs</td>-->
<!--                <td>Parent Blogs</td>-->
<!--                <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>-->
<!--            </tr>-->
        </table>
    </div>
</div>
<?php require "footer.php";?>
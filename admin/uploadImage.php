<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

if(!empty($_FILES)) {
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    $name = generateRandomString(8);
    if (is_uploaded_file($_FILES['file']['tmp_name'])) {
        $type_arr = explode('/', $_FILES['file']['type']);
        $type = $type_arr[1];
        $orig_name = $_FILES['file']['name'];

        move_uploaded_file($_FILES['file']['tmp_name'], __DIR__ . "/../uploads/$name.$type");
        $json = file_get_contents(__DIR__ . '/plugins/redactor/core/uploadImage/images.json');
        $str_set = mb_substr($json, 0, -2);
        $str = ",\n
{
    \"thumb\": \"/uploads/$name.$type\",
    \"image\": \"/uploads/$name.$type\",
    \"id\": \"$name\",
    \"title\": \"$orig_name\"
}";

        file_put_contents(__DIR__ . "/plugins/redactor/core/uploadImage/images.json", $str_set . $str . "\n]");

    }

}

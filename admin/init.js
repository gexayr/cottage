$('#redactor').redactor({
    imageUpload: 'uploadImage.php',
    imageManagerJson: '/admin/plugins/redactor/core/uploadImage/images.json',
    // fileUpload: '/admin/Assets/js/plugins/redactor/core/uploadFile/uploadFile.php',
    // fileManagerJson: '/admin/Assets/js/plugins/redactor/core/files.json',
    plugins: ['table', 'video', 'source','imagemanager', 'filemanager'],
    imagePosition: true,
    imageResizable: true

});

function removePage(id, element){
    if(!confirm('Do you want to delete the page with id '+ id + '?')){
        return false;
    }
    var formData = new FormData();
    formData.append('id', id);

    $.ajax({
        url: '/admin/remove.php',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
            $('#waiting').css('display', 'block');
        },
        success: function (result) {
            console.log(result);
            if(result == 1){
                $(element).parent().parent().fadeOut();
            }else{
                alert(result);
            }
            $('#waiting').css('display', 'none');

        }
    });
}
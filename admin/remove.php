<?php

require "../pdo/config.php";

if(isset($_POST['id'])) {
    try {
        $connection = new PDO($dsn, $username, $password, $options);
        $id = $_POST['id'];
        $sql = "DELETE FROM pages WHERE id =  :id";

        $stmt = $connection->prepare($sql);
        $stmt->bindParam(':id', $_POST['id'], PDO::PARAM_INT);
        $stmt->execute();
        echo 1;
    } catch(PDOException $error) {
        echo $error->getMessage();
    }

}
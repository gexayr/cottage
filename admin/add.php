<?php
require "header.php";
require "../pdo/config.php";
require "../pdo/common.php";
//dd(__DIR__);
try  {
    $connection = new PDO($dsn, $username, $password, $options);

    $sql = "SELECT * 
            FROM pages
            WHERE parent = :parent";

    $parent = 0;
    $statement = $connection->prepare($sql);
    $statement->bindParam(':parent', $parent, PDO::PARAM_STR);
    $statement->execute();

    $result = $statement->fetchAll();
} catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
?>
    <div class="container">
        <div class="row">

            <h2 align="center">Добавление Страницы</h2>
            <form class="form-horizontal" action="create.php" method="post">
                <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
                <div class="form-group">
                    <label for="usr">Загаловок:</label>
                    <input type="text" class="form-control" name="title" required>
                </div>
                <div class="form-group">
                    <label for="usr">Адрес:</label>
                    <input type="text" class="form-control" name="alias" placeholder="/example" required>
                    <p>* если хотите добавить страницу в подкатегорию существующей статической страницы (НА ГЛАВНУЮ,
                        О КОМПАНИИ, НОВОСТИ, ПРОЕКТИРОВАНИЕ, СТРОИТЕЛЬСТВО, ФАСАДНЫЕ РАБОТЫ, КОНТАКТЫ), то напишите адрес
                        похожим на родительскую. (напримаер для страницы СТРОИТЕЛЬСТВО нужно написать stroitelstvo1 или stroitelstvo_test)</p>
                </div>
                <div class="form-group">
                    <label for="usr">Родительская страница:</label>
                    <select name="parent" class="form-control">
                        <option value="">Без Родительской Страницы</option>
                        <option value="2">Статическая страница</option>
                        <?php foreach ($result as $item) { ?>
                            <option value="<?=$item['id']?>"><?=$item['title']?></option>
                        <?php }?>
                    </select>
                    <p>* для динамических страниц достаточно только выбрать родительскую страницу, адрес не обязательно
                        должно быть похожим на адрес родительской страницы </p>
                </div>
                <div class="form-group">
                    <label for="usr">Добавит в меню? </label>
                    <input type="checkbox" name="menu">
                </div>
                <div class="form-group">
                    <label>Контент</label>
                    <textarea name="content" id="redactor"></textarea>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 pull-right">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

<?php require "footer.php"; ?>
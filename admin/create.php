<?php
require "header.php";

require "../pdo/config.php";
require "../pdo/common.php";

//echo "<pre>";
//print_r($_POST);
//echo "</pre>";
//die;
if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

try  {
    $connection = new PDO($dsn, $username, $password, $options);
    $menu=0;
    $parent=0;
    if(isset($_POST['menu']) && $_POST['menu'] == 'on')$menu=1;
    if($_POST['parent'] != '')$parent=$_POST['parent'];
    $new_page = array(
        "title" => $_POST['title'],
        "alias"  => $_POST['alias'],
        "menu"     => $menu,
        "parent"     => $parent,
        "content"     => $_POST['content'],
    );

    $sql = sprintf(
        "INSERT INTO %s (%s) values (%s)",
        "pages",
        implode(", ", array_keys($new_page)),
        ":" . implode(", :", array_keys($new_page))
    );

    $statement = $connection->prepare($sql);
    $statement->execute($new_page);

    echo '     <div class="alert alert-success alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            Вы <strong>Успешно</strong> добавили страницу.
        </div>';
} catch(PDOException $error) {
//        echo $sql . "<br>" . $error->getMessage();
    echo '     <div class="alert alert-danger alert-dismissible fade in col-sm-6 col-sm-offset-3" style="text-align: center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> '.$error->getMessage().'
        </div>';


}
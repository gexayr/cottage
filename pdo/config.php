<?php
/**
 * Configuration for database connection
 *
 */

$host       = "localhost";
$username   = "root";
$password   = "password";
$dbname     = "kotej";
$dsn        = "mysql:host=$host;dbname=$dbname;charset=utf8";
$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
//    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
              );


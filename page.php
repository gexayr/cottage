<?php
require 'header.php';
require "pdo/config.php";
//$alias = "/".$_SERVER['QUERY_STRING'];
$alias = $_SERVER['REQUEST_URI'];
try  {
    $connection = new PDO($dsn, $username, $password, $options);
    $sql = "SELECT * 
            FROM pages
            WHERE alias = :alias";
    $statement = $connection->prepare($sql);
    $statement->bindParam(':alias', $alias, PDO::PARAM_STR);
    $statement->execute();
    $result = $statement->fetchAll();
} catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
if ($result && $statement->rowCount() > 0) {
    $page = $result[0];
}else{
    echo "<script>window.location.href = '/404.php'</script>";
//    header('location:404.php');
}

echo "<div class='col-sm-12 page'>";
echo $page['content'];
echo "</div>";

require 'footer.php';

<!--end container-->
</div>
<footer class="footer container-fluid">
    <div class="container footer_block">
        <div class="row">
            <div class="col-sm-12">
                <div class="text-V12 col-md-4 footer-text"><div>1995 - (февраль) <?php echo date("Y"); ?></div>
                </div>
                <div class="text-V12">

                    <!-- Yandex.Metrika informer -->
                                  <a href="http://metrika.yandex.ru/stat/?id=20955442&amp;from=informer"
                    target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/20955442/3_0_FFFFF9FF_EDEAD9FF_0_pageviews"
                    style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onClick="try{Ya.Metrika.informer({i:this,id:20955442,lang:'ru'});return false}catch(e){}"/></a>
                     /Yandex.Metrika informer

                     Yandex.Metrika counter
                                      <script type="text/javascript">
                    (function (d, w, c) {
                        (w[c] = w[c] || []).push(function() {
                            try {
                                w.yaCounter20955442 = new Ya.Metrika({id:20955442,
                                        webvisor:true,
                                        clickmap:true,
                                        trackLinks:true,
                                        accurateTrackBounce:true});
                            } catch(e) { }
                        });

                        var n = d.getElementsByTagName("script")[0],
                            s = d.createElement("script"),
                            f = function () { n.parentNode.insertBefore(s, n); };
                        s.type = "text/javascript";
                        s.async = true;
                        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                        if (w.opera == "[object Opera]") {
                            d.addEventListener("DOMContentLoaded", f, false);
                        } else { f(); }
                    })(document, window, "yandex_metrika_callbacks");
                                      </script>
                                      <noscript>
                                      <div><img src="//mc.yandex.ru/watch/20955442" style="position:absolute; left:-9999px;" alt="" /></div>
                                      </noscript>
                    <!-- /Yandex.Metrika counter -->
                </div>
                <div align="right" class="red-V12 col-md-8 footer-text"><div>&copy;Запрещено копирование и перепечатка материалов сайта</div>
                    <button type="button" class="btn-feedback" data-toggle="modal" data-target="#myModal" id="modal_button">Обратная связь</button>
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align: center">Форма обратной связи</h4>
            </div>
            <form action="/feedback.php" method="post" accept-charset="utf-8">
                <div class="modal-body">
                    <div class="modal-body" style="padding: 5px;">
                        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                <input class="form-control width_90" name="name" placeholder="Укажите Ваше имя" type="text" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                <input class="form-control width_90" name="email" placeholder="Укажите Ваш E-mail" type="email" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                <input class="form-control width_90" name="phone" placeholder="Укажите Ваш телефон (при желании)" type="tel" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <textarea style="resize:vertical;" class="form-control width_90" placeholder="Введите сообщение..." rows="6" name="message" required></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="g-recaptcha" data-sitekey="6Ldm1qkUAAAAAFSNaGAlCU8GfxXLHe4gwlFRIQAP" data-callback="enableBtn"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-default" id="button1" value="Отправить"/>
                    <input type="reset" class="btn btn-default" value="Очистить" />
                    <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Закрыть</button>
                </div>
            </form>
            <script>
                document.getElementById("button1").disabled = true;
                function enableBtn(){
                    document.getElementById("button1").disabled = false;
                }
            </script>
        </div>

    </div>
</div>
<!--End Modal-->


</body>
</html>
